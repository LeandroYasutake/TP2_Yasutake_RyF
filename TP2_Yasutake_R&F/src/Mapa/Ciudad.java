package Mapa;

import java.io.Serializable;
import java.util.Objects;

public class Ciudad implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2591664355930712536L;
	private String nombre;
    private double latitud;
    private double longitud;
    private int identificador;
    
    public Ciudad(String nombre, double latitud, double longitud)
    {
       setNombre(nombre);
       setLatitud(latitud);
       setLongitud(longitud);
    }

	public String getNombre() 
	{
		return nombre;
	}

	public void setNombre(String nombre) 
	{
		if (nombre == "")
            throw new IllegalArgumentException("El nombre de la ciudad no puede ser vacio");
		
		this.nombre = nombre;
	}

	public double getLatitud() 
	{
		return latitud;
	}

	public void setLatitud(double latitud) 
	{
		if (latitud > 90 || latitud < -90)
	            throw new IllegalArgumentException("La latitud debe estar comprendida entre 90 Y -90 grados");
		
		this.latitud = latitud;
	}

	public double getLongitud() 
	{
		return longitud;
	}

	public void setLongitud(double longitud) 
	{
		if (latitud > 180 || latitud < -180)
            throw new IllegalArgumentException("La latitud debe estar comprendida entre 180 Y -180 grados");
		
		this.longitud = longitud;
	}

	public int getIdentificador() 
	{
		return identificador;
	}

	public void setIdentificador(int identificador) 
	{
		this.identificador = identificador;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Ciudad other = (Ciudad) obj;
		
		return (Objects.equals(nombre, other.getNombre()));
	}
	
	@Override
    public String toString()
	{
		return this.nombre;
	}

}
