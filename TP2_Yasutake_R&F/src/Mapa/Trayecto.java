package Mapa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Trayecto implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1377032596697755519L;
	private ArrayList<Ruta> caminoMinimo;
	private Set<Ciudad> ciudadesVisitadas;
	private double peajesTotales;
	private double distanciaTotal;
	private String origen;
	private String destino;
	
	public Trayecto () 
	{
		this.peajesTotales = 0;
		this.distanciaTotal = 0;
		this.ciudadesVisitadas = new HashSet<Ciudad>();
		this.caminoMinimo = new ArrayList<Ruta>();
	}

	public double getPeajesTotales() 
	{
		return peajesTotales;
	}
	
	public double getDistanciaTotal() 
	{
		return distanciaTotal;
	}
	
	public int cantSubTrayectos()
	{
		return caminoMinimo.size();
	}
	
	public ArrayList<Ruta> getCaminoMinimo()
	{
		return caminoMinimo;
	}
	
	public Set<Ciudad> getCiudadesVisitadas()
	{
		return this.ciudadesVisitadas;
	}
	
	public String getOrigen() 
	{
		return origen;
	}

	public void setOrigen(String origen) 
	{
		this.origen = origen;
	}

	public String getDestino() 
	{
		return destino;
	}

	public void setDestino(String destino) 
	{
		this.destino = destino;
	}
	
	public void addSubTrayecto(Ruta ruta) 
	{
		if (ruta.getPeaje() == 1)
			peajesTotales++;
		distanciaTotal += ruta.getDistancia();
		ciudadesVisitadas.add(ruta.getOrigen());
		ciudadesVisitadas.add(ruta.getDestino());
		caminoMinimo.add(ruta);
	}
	
	@Override
    public String toString()
    {
        return "Or.: "+this.origen+" - "+"Des.: "+this.destino;
    }
}
