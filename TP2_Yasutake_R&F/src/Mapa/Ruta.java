package Mapa;

import java.io.Serializable;

public class Ruta implements Serializable
{
	 /**
	  * 
	  */
	 private static final long serialVersionUID = 1717142021010606243L;
	 private Ciudad origen;
	 private Ciudad destino;
	 private double distancia;
	 private int peaje;
	 
	 public Ruta(Ciudad origen, Ciudad destino, double distancia, int hayPeaje)
	 {
		 setOrigen(origen);
		 setDestino(destino);
		 setDistancia(distancia);
		 setPeajes(hayPeaje);
	 }

	public Ciudad getOrigen() 
	{
		return origen;
	}

	public void setOrigen(Ciudad origen) 
	{
		this.origen = origen;
	}

	public Ciudad getDestino() 
	{
		return destino;
	}

	public void setDestino(Ciudad destino) 
	{
		this.destino = destino;
	}

	public double getDistancia() 
	{
		return distancia;
	}

	public void setDistancia(double distancia) 
	{
		if (distancia <= 0)
            throw new IllegalArgumentException("La distancia no puede ser negativa o nula");
		
		this.distancia = distancia;
	}

	public double getPeaje() 
	{
		return peaje;
	}

	public void setPeajes(int peaje) 
	{
		this.peaje = peaje;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Ruta other = (Ruta) obj;
		
		return (origen == other.getOrigen() && destino == other.getDestino() || 
				origen == other.getDestino() && destino == other.getOrigen());
	}
	
	@Override
	public String toString()
	{
		return "Or.: " + origen.toString() + " - Des.: " + destino.toString();
	}
}
