package Mapa;

import java.io.Serializable;
import java.util.List;
import java.util.Stack;

public class Constructor implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3216452442745720796L;

	public static Trayecto generarTrayectoList(List<Integer> ciudades, Ruta [][]matrizRutasCiudades) 
		{
			Trayecto trayectoMinimo = new Trayecto();
	    	
	    	for (int i = 0; i < ciudades.size()-1; i++)
	    	{
	    		try 
	    		{
	    			trayectoMinimo.addSubTrayecto(matrizRutasCiudades[ciudades.get(i)][ciudades.get(i+1)]);
				} catch (Exception e) 
	    		{
					//no hubo camino minimo posible
				}	
			}
	    	
	    	return trayectoMinimo;
	    }
	
	public static Trayecto generarTrayectoStack(Stack<Integer> ciudades, Ruta [][]matrizRutasCiudades) 
    {
    	Trayecto trayectoMinimo = new Trayecto();
    	
    	for (int i = 0; i < ciudades.size()-1; i++)
    	{
    		try 
    		{
    			trayectoMinimo.addSubTrayecto(matrizRutasCiudades[ciudades.get(i)][ciudades.get(i+1)]);
			} catch (Exception e) 
    		{
				//no hubo camino minimo posible
			}	
		}
    	
    	return trayectoMinimo;
    }
}
