package Mapa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import Grafos.CaminoFuerzaBruta;
import Grafos.Dijkstra;
import Grafos.GrafoPesado;

public class Mapa implements Serializable
{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2582811704983334654L;
	private ArrayList<Ciudad> ciudades;
    private ArrayList<Ruta> rutas;
    private ArrayList<Trayecto> rutasFinales;
    private Ruta[][] matrizRutasCiudades;
    private int id;
    private int rutasSinPeaje;
    private int rutasConPeaje;
   
    public Mapa()
    {
        this.ciudades = new ArrayList<Ciudad>();
        this.rutas = new ArrayList<Ruta>();
        this.rutasFinales = new ArrayList<Trayecto>();
        this.rutasSinPeaje = 0;
        this.rutasConPeaje = 0;
        this.id = 0;
    }
    
    public Ruta getRuta(int k)
    {
        return rutas.get(k);
    }
    
    public ArrayList<Ruta> getRutas()
    {
    	return rutas;
    }

    public int cantRutas()
    {
        return rutas.size();
    }
    
    public int getRutasSP()
    {
    	return this.rutasSinPeaje;
    }
 
    public int getRutasCP()
    {
    	return this.rutasConPeaje;
    }
    
    public ArrayList<Trayecto> getTrayectos()
    {
    	return this.rutasFinales;
    }
    
    public Ruta[][] getMatrizRutasCiudades() 
    {
		return this.matrizRutasCiudades;
	}
    
    public Ciudad getCiudad(int k)
    {
        return ciudades.get(k);
    }
    
    public ArrayList<Ciudad> getCiudades()
    {
    	return ciudades;
    }
    
    public int cantCiudades()
    {
        return ciudades.size();
    }
    
    public int getId()
    {
    	return this.id;
    }
    
    public void addCiudad(Ciudad ciudad)
    {
    	if (!ciudades.contains(ciudad))
    	{
    		ciudad.setIdentificador(id);
            ciudades.add(ciudad);
            id++;
    	}
    }
    
    public void removeCiudad(Ciudad ciudad)
    {
    	if (ciudades.contains(ciudad) && ciudades.size() >= 1)
    	{
        	controlarRutas(ciudad);
        	ciudades.remove(ciudad);
    	}
    	else
    		throw new IllegalArgumentException("No existen ciudades para remover.");
    }

    private void controlarRutas(Ciudad ciudad) 
    {
    	Iterator<Ruta> ite = rutas.iterator();
    	
    	while(ite.hasNext()) 
    	{
    		Ruta otherRuta = ite.next();
    	
    		if(otherRuta.getOrigen().equals(ciudad ) || otherRuta.getDestino().equals(ciudad))
    		{
    			if(otherRuta.getPeaje() == 1)
            		rutasConPeaje--;
            	else
            		rutasSinPeaje--;
    		
    			ite.remove();
    		}
    	}
	} 

    public void addRuta(Ruta ruta)
    {
    	if(!this.rutas.contains(ruta))
    	{
    		rutas.add(ruta);
        	
    		if(ruta.getPeaje() == 1)
        		rutasConPeaje++;
        	else
        		rutasSinPeaje++;
    	}
    	
    	else
    		throw new IllegalArgumentException("Ruta existente!");
    }
    
    public void removeRuta(Ruta ruta)
    {
    	if (rutas.contains(ruta) && rutas.size() >= 1)
    	{
        	rutas.remove(ruta);
        	if(ruta.getPeaje() == 1)
        		rutasConPeaje--;
        	else
        		rutasSinPeaje--;
    	}
    	else
    		throw new IllegalArgumentException("No existen rutas para remover.");
    }  
    
    public Trayecto caminoMinimo(int idOrigen, int idDestino, int peajesMaximos)
    {	
    	GrafoPesado grafoConDistancias = crearGrafoCiudadesYrutas(cantCiudades());
    	Trayecto camMin = caminoMinimoDK(idOrigen, idDestino, grafoConDistancias);
    	
    	if (camMin.getPeajesTotales() <= peajesMaximos)
    	{
    		if(camMin.getDistanciaTotal() == 0)
    			throw new IllegalArgumentException("No hubo camino posible");
    		rutasFinales.add(camMin);
    		camMin.setOrigen(getCiudad(idOrigen).toString());
    		camMin.setDestino(getCiudad(idDestino).toString());
    		return camMin;
    	}
    		
    	else
    	{
    		Trayecto camMinP = caminoMinimoFuerzaBruta(idOrigen, idDestino, peajesMaximos, grafoConDistancias);
    		if(camMinP.getDistanciaTotal() == 0)
    			throw new IllegalArgumentException("No hubo camino posible");
    		rutasFinales.add(camMinP);
    		camMinP.setOrigen(getCiudad(idOrigen).toString());
    		camMinP.setDestino(getCiudad(idDestino).toString());
    		return camMinP;
    	}
    }
	
	private Trayecto caminoMinimoDK(int idOrigen, int idDestino, GrafoPesado grafoConDistancias) 
    {
    	List<Integer> idCiudadesMinimas;
    	Dijkstra camMinGrafo = new Dijkstra (grafoConDistancias);
    	Trayecto camMin;
    
    	idCiudadesMinimas = camMinGrafo.caminoMinimo(idOrigen, idDestino);
    	camMin = Constructor.generarTrayectoList(idCiudadesMinimas, getMatrizRutasCiudades());
    	
    	return camMin;
    }
	
	private Trayecto caminoMinimoFuerzaBruta(int idOrigen, int idDestino, int peajesMaximos, GrafoPesado grafoConDistancias)
	{
    	Trayecto camMinPeajesFinal = null;
    	
    	CaminoFuerzaBruta camMinFuerzaBruta = new CaminoFuerzaBruta(peajesMaximos, grafoConDistancias, this.matrizRutasCiudades);
    	camMinFuerzaBruta.encontrarTrayectoMinimoFuerzaBruta(idOrigen, idDestino);
    			
    	if (camMinFuerzaBruta.getCaminoMinimo() != null)
    		camMinPeajesFinal = camMinFuerzaBruta.getCaminoMinimo();
		
    	return camMinPeajesFinal;
	}
	
	private GrafoPesado crearGrafoCiudadesYrutas(int tama�o) 
	{
		GrafoPesado grafo = new GrafoPesado(tama�o);
		matrizRutasCiudades = new Ruta[tama�o][tama�o];
		int origen = 0;
		int destino = 0;
		
		for (Ruta ruta : rutas)
        {
			origen = getPosCiudad(ruta.getOrigen());
            destino = getPosCiudad(ruta.getDestino());
            matrizRutasCiudades[origen][destino] = ruta;
           	matrizRutasCiudades[destino][origen] = ruta;
           	grafo.agregarArista(origen, destino, ruta.getDistancia());
        }
		return grafo;
	}
	
	public int getPosCiudad(Ciudad ciudad)
	{
		for (int i = 0; i < cantCiudades(); i++)
            if (getCiudad(i).equals(ciudad))
                return i;

        throw new IllegalArgumentException("No existe la ciudad en el mapa");
	}

	public void resetearMapa(boolean todo)
    {
    	if(todo)
    	{
	    	ciudades = new ArrayList<Ciudad>();
	        rutas = new ArrayList<Ruta>();
	        id = 0;
    	}
    	else
    		rutas = new ArrayList<Ruta>();
    }
    
    public int numeroVecinos(Ciudad ciudad)
    {
    	int ret = 0;
    	
    	for (Ruta ruta : rutas)
    		if (ruta.getOrigen().equals(ciudad) || ruta.getDestino().equals(ciudad))
    			ret++;
    	
    	return ret;
    }
    
    public String getVecinos(Ciudad ciudad)
    {
    	String ret = "";
    	
    	for (Ruta ruta : rutas)
    	{
    		if (ruta.getOrigen().equals(ciudad))
    			ret += " "+ruta.getDestino().toString();
    	
    		else if (ruta.getDestino().equals(ciudad))
    			ret += " "+ruta.getOrigen().toString();
    	}
    	return ret;
    }
    
    public ArrayList<Ciudad> getVecinosArray(Ciudad ciudad)
    {
    	ArrayList<Ciudad> ret = new ArrayList<Ciudad>();
    	
    	for (Ruta ruta : rutas)
    	{
    		if (ruta.getOrigen().equals(ciudad))
    			ret.add(ruta.getDestino());
    	
    		else if (ruta.getDestino().equals(ciudad))
    			ret.add(ruta.getOrigen());
    	}
    	return ret;
    }
    
    public double getDistanciaReal(Ciudad origen, Ciudad destino) 
    {
    	int tierraRadio = 6371;
    	double distancia = 0;
    	double distanciaLat = Math.toRadians(destino.getLatitud() - origen.getLatitud());
        double distanciaLong = Math.toRadians(destino.getLongitud() - origen.getLongitud());
       
        double a = Math.sin(distanciaLat / 2) * Math.sin(distanciaLat / 2)
                + Math.cos(Math.toRadians(origen.getLatitud())) * Math.cos(Math.toRadians(destino.getLatitud()))
                * Math.sin(distanciaLong / 2) * Math.sin(distanciaLong / 2);
        
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        
        distancia = tierraRadio * c;

        distancia = Math.pow(distancia, 2) + Math.pow(0, 2);
        
        return Math.sqrt(distancia);
    }
}
