package Grafos;

import java.io.Serializable;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

import Mapa.Constructor;
import Mapa.Ruta;
import Mapa.Trayecto;

public class CaminoFuerzaBruta implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1109809594465610518L;
	private double distanciaMinima;
	private double maxPeajes;
	private GrafoPesado grafo;
	private Ruta[][] matrizRutasCiudades;
	private Trayecto caminoMinimo;
	
	public CaminoFuerzaBruta(double cantMaxPeajes, GrafoPesado grafoConDistancias, Ruta[][] matrizRutasCiudades)
	{
		this.distanciaMinima = Double.MAX_VALUE;
		this.maxPeajes = cantMaxPeajes;
		this.grafo = grafoConDistancias;
		this.matrizRutasCiudades = matrizRutasCiudades;
		this.caminoMinimo = null;
	}
	
	public Trayecto getCaminoMinimo() 
	{
		return caminoMinimo;
	}

	private void setCaminoMinimo(Trayecto caminoMinimo) 
	{
		this.caminoMinimo = caminoMinimo;
	}
	
	private double getDistanciaMinima() 
	{
		return distanciaMinima;
	}

	private void setDistanciaMinima(double distanciaMinima) 
	{
		this.distanciaMinima = distanciaMinima;
	}

	private double getMaxPeajes() 
	{
		return maxPeajes;
	}

	private GrafoPesado getGrafo()
	{
		return grafo;
	}

	private Ruta[][] getMatrizRutasCiudades() 
	{
		return matrizRutasCiudades;
	}
	
    public void encontrarTrayectoMinimoFuerzaBruta(int origen, int destino) 
    {
    	Stack<Integer> ciudades = new Stack<Integer>();
        
    	ciudades.push(origen);
        recorrerRutas(origen, destino, ciudades);
    }
   
    private void recorrerRutas(int origen, int destino, Stack<Integer> ciudades) 
    {
    	Trayecto camMinPosible = Constructor.generarTrayectoStack(ciudades, getMatrizRutasCiudades());

    	if(origen == destino) 
        {
    		double distanciaTotal = evaluarDistanciaTotal(camMinPosible);
           
            if(distanciaTotal < getDistanciaMinima() && camMinPosible.getPeajesTotales() <= getMaxPeajes()) 
            {
            	setDistanciaMinima(distanciaTotal); 
                setCaminoMinimo(camMinPosible);
            }
            
            return;
        }
        
    	if (camMinPosible.getPeajesTotales() > getMaxPeajes())
    		return;
    	
        List<Integer> vecinosSinEvaluar = new Vector<Integer>();
       
        agregarVecinosSinEvaluar(vecinosSinEvaluar, ciudades, origen);
        
        recorrerRutasVecinas(vecinosSinEvaluar, ciudades, destino); 
    }
    
    private double evaluarDistanciaTotal(Trayecto camMinPosible) 
    {
    	if (camMinPosible.getDistanciaTotal() == 0)
    		return Double.MAX_VALUE;
    	else
    		return camMinPosible.getDistanciaTotal();
    }
    
	private void agregarVecinosSinEvaluar(List<Integer> vecinosSinEvaluar, Stack<Integer> resultado, int origen)
    {
    	for(int vecino : getGrafo().vecinos(origen))
        	if (!resultado.contains(vecino))
        		vecinosSinEvaluar.add(vecino);
    }
	
	private void recorrerRutasVecinas(List<Integer> vecinosSinEvaluar, Stack<Integer> ciudades, int destino) 
	{
		for(int vecinoNoEvaluado: vecinosSinEvaluar) 
        {
			ciudades.push(vecinoNoEvaluado);
            recorrerRutas(vecinoNoEvaluado, destino, ciudades);
            ciudades.pop();
        }
	}
}
