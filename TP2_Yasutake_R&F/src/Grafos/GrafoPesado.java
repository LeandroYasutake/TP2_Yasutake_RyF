package Grafos;

import java.io.Serializable;
import java.util.Set;

public class GrafoPesado implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3829107192855663232L;
	private Grafo grafo;
	private double [][] pesos;

	public GrafoPesado(int n) 
	{
		grafo = new Grafo(n);
		pesos = new double[n][n];
	}

	public void agregarArista(int i, int j, double peso) 
	{
		grafo.agregarArista(i, j);
		pesos[i][j] = peso;	
		pesos[j][i] = peso;
	}
	
	public int vertices()
	{
		return grafo.cantidadDeVertices();
	}

	public boolean contieneArista(int i, int j) 
	{
		return grafo.contieneArista(i, j);
	}

	public double pesoArista(int i, int j) 
	{
		return pesos[i][j];
	}
	
	public Set<Integer> vecinos(int k)
	{
		return grafo.vecinos(k);
	}
	
	@Override
	public String toString()
	{
		return this.grafo.toString();
	}	

}

