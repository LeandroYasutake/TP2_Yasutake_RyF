package Grafos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class Dijkstra implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7951280669153604803L;
	private double[] distanciaTentativas;
	private ArrayList<Integer> cola;
	private int[] verticesPrevios;
	private boolean visitados[];
	private GrafoPesado grafoAux;
	
	public Dijkstra (GrafoPesado grafoConDistancias)
	{
		distanciaTentativas = new double[grafoConDistancias.vertices()];
		verticesPrevios = new int[grafoConDistancias.vertices()];
		Arrays.fill(verticesPrevios, -1);
		visitados = new boolean[grafoConDistancias.vertices()];
		cola = new ArrayList<Integer>();
		grafoAux = grafoConDistancias;
		
		for (int i = 0 ; i < grafoConDistancias.vertices() ; i++)
		{
            distanciaTentativas[i] = Double.MAX_VALUE;
			visitados[i] = false;
		}	
	}
	
	public List<Integer> caminoMinimo(int origen, int destino)
	{
		dijkstra(origen);
		return camino(origen, destino);
	}
	
	public void dijkstra(int inicial)
	{
		cola.add(inicial);
		distanciaTentativas[inicial] = 0;      
	   
		int actual; 
		double peso;
	    
	    while(!cola.isEmpty())
	    {                   								
	        actual = vecinoPesoMenor(cola); 
	        cola.remove(damePosicion(actual));
	        
	        if (!visitados[actual])
	        {
	        	visitados[actual] = true;
		        
		        for (int vecino : grafoAux.vecinos(actual))						 			
		        { 
		            peso = grafoAux.pesoArista(actual, vecino);       		 			
		           
		            if(!visitados[vecino])      								
		                relajacion(actual, vecino, peso);						 
		        }
	        }
	    }
	}
	
	public int damePosicion(int vecino)
	{
		for (int i = 0 ; i < cola.size() ; i++)
        	if (vecino == cola.get(i))
        		return i;
		
		return 0;
	}
	
	public int vecinoPesoMenor(ArrayList<Integer> cola) 
	{
		double min = Double.MAX_VALUE;
		int vecinoRet = 0;
		
		for(int vecino : cola)
			if (distanciaTentativas[vecino] < min)
			{
				min = distanciaTentativas[vecino];
				vecinoRet = vecino;
			}
		
		return vecinoRet;
	}
	
	public void relajacion(int actual , int vecino , double peso)
	{
		if (distanciaTentativas[actual] + peso < distanciaTentativas[vecino])
		{
			distanciaTentativas[vecino] = distanciaTentativas[actual] + peso;
			verticesPrevios[vecino] = actual;
			cola.add(vecino); 														
		}
	}
	
	private List<Integer> camino(int origen, int destino)
    {
		List<Integer> ret = new ArrayList<Integer>();
        int siguienteNodo = destino;
        
       if (verticesPrevios[destino] == -1)
       {
    	   return ret;
       }
    	  
 
        while (siguienteNodo != origen)
        {
        	ret.add(siguienteNodo);
        	siguienteNodo = verticesPrevios[siguienteNodo];
        }
        
        ret.add(origen);
        Collections.reverse(ret);
        return ret;
    }
}




