package Grafos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


public class Grafo implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4062334932861996874L;
	private ArrayList<Set<Integer>> grafo;
	private int vertices;
	private int aristas;
	
	public Grafo(int n)
	{
		if (n < 0)
			throw new IllegalArgumentException("Cantidad de vertices negativo " + n);
		
		vertices = n;
		aristas = 0;
		grafo = new ArrayList<Set<Integer>>();
		
		for (int i = 0 ; i < vertices ; i++)
			grafo.add(new HashSet<Integer>());
	}
	
	public void agregarArista(int i, int j)
	{
		chequearExtremos(i,j);

		if (!contieneArista(i,j))
		{
			grafo.get(i).add(j);
			grafo.get(j).add(i);
			aristas++;
		}
	}
	
	public void removerArista(int i,int j)
	{
		chequearExtremos(i,j);

		if(contieneArista(i,j))
		{
			grafo.get(i).remove(j);
			grafo.get(j).remove(i);
			aristas--;
		}
	}
	
	public boolean contieneArista(int i, int j)
	{
		chequearExtremos(i,j);
		
		return grafo.get(i).contains(j);
	}
	
	public int grado(int vertice)
	{
		chequearVertice(vertice);
		
		int ret = 0;
		for (int i = 0 ; i < vertices; i++)
			if (vertice != i && contieneArista(vertice, i))
				ret++;
		
		return ret;		
	}
	
	public Set<Integer> vecinos(int vertice)
	{
		chequearVertice(vertice);
		
		Set<Integer> ret = new HashSet<Integer>();
		
		for (int i = 0 ; i < vertices ; i++)
			if (vertice != i && contieneArista(vertice,i))
				ret.add(i);
		
		return ret;
	}	
	
	public int cantidadDeAristas()
	{
		return aristas;
	}

	private void chequearVertice(int vertice) 
	{
		if (vertice <= -1 || vertice >= vertices)
			throw new IllegalArgumentException("Vertice fuera de rango");
	}

	private void chequearExtremos(int i, int j) 
	{
		if (i <= -1 || i >= vertices || j <=-1 || j >= vertices)
			throw new IllegalArgumentException("Vertices fuera de rango");
		
		if (i == j)
			throw new IllegalArgumentException("Los vertices deben ser distintos");
	}

	public int cantidadDeVertices() 
	{
		return vertices;
	}
	
	public boolean sonVecinos(int i, int j) 
	{
		return contieneArista(i,j);
	}

	@Override
	public String toString()
	{
		String grafo = "";
		for(int i = 0; i < this.grafo.size(); i++)
			grafo += "Vertice: " + i + " Vecinos: " + vecinos(i) + "\n";
		return grafo;
	}
}
