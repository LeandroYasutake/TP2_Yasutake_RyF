package Datos;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


import Mapa.Mapa;


public class FileManager 
{
	public static void guardarMapa(Mapa mapa)
    {
    	try 
    	{
	    	FileOutputStream fos = new FileOutputStream("Archivos\\Mapa.txt");
	    	ObjectOutputStream out = new ObjectOutputStream(fos);
	    	out.writeObject(mapa);
	    	out.close();
    	}catch (Exception ex){}
    }
	
	public static Mapa cargarMapa()
	{
		Mapa mapa = null;
		try 
    	{
			FileInputStream s = new FileInputStream("Archivos\\Mapa.txt");
			ObjectInputStream in = new ObjectInputStream(s);
		
			mapa = (Mapa) in.readObject();
			
			in.close();
    	}catch (Exception ex){};
		
		return mapa;
	}
	
	public static Mapa cargarMapaEjemplo()
	{
		Mapa mapa = null;
		try 
    	{
			FileInputStream s = new FileInputStream("Archivos\\MapaEjemplo.txt");
			ObjectInputStream in = new ObjectInputStream(s);
		
			mapa = (Mapa) in.readObject();
			
			in.close();
    	}catch (Exception ex){}
		
		return mapa;
	}
}
