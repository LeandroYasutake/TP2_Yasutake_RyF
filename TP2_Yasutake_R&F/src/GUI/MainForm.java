package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.openstreetmap.gui.jmapviewer.JMapViewer;

import Datos.FileManager;
import Mapa.Mapa;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import java.awt.Font;

public class MainForm extends JFrame 
{
	private static final long serialVersionUID = -6300070372636419169L;
	private JPanel contentPane;
	private JMapViewer mapaPantalla;
	private Mapa grafoMapa;
	private VentanaAddCiudad ventanaAgregarCiudad;
	private VentanaRemoveCiudad ventanaRemoveCiudad;
	private VentanaAddCiudadManual ventanaAgregarCiudadManual;
	private VentanaAddRuta ventanaAgregarRuta;
	private VentanaRemoveRuta ventanaRemoveRuta;
	private VentanaCaminoMinimo ventanaCaminoMinimo;
	private VentanaInfoCiudades ventanaInfoCiudad;
	private VentanaInfoCaminosMinimos ventanaInfoCaminosMinimos;
	private VentanaInfoRutas ventanaInfoRutas;
	private VentanaAyuda ventanaAyuda;
	private JMenuBar menu;
	private JMenu archivoMenu;
	private JMenu mapaMenu;
	private JMenu ciudadesMenu;
	private JMenu rutasMenu;
	private JMenu consultasMenu;
	private JMenu ayudaMenu;
	private JMenuItem guardarMenu;
	private JMenuItem cargarMenu;
	private JMenuItem salirMenu;
	private JMenuItem resetearMapaMenu;
	private JMenuItem quitarRutasMenu;
	private JMenuItem agregarCiudadMenu;
	private JMenuItem removerCiudadMenu;
	private JMenuItem agregarRutaMenu;
	private JMenuItem removerRutaMenu;
	private JMenuItem caminoMinimo;
	private JMenuItem infoCiudadesMenu;
	private JMenuItem infoRutasMenu;
	private JMenuItem infoCaminosMinimosMenu;
	private JMenuItem cargarEjemploMenu;
	private JMenuItem informacionMenu;
	private JMenuItem acercaDeMenu;

	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try {
					MainForm frame = new MainForm();
					frame.setVisible(true);
					UIManager.setLookAndFeel(
					UIManager.getSystemLookAndFeelClassName());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainForm() 
	{
		inicializarGrafo();
		inicializarPantalla();
		inicializarMapa();
	}

	private void inicializarGrafo() 
	{
		grafoMapa = new Mapa();
	}

	private void inicializarPantalla()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		contentPane.requestFocusInWindow();
		setTitle("Proyecto: El c�mino m�s corto");
		setMenus();
	}
	
	private void setMenus() 
	{
		menu = new JMenuBar();
		setJMenuBar(menu);
		
		archivoMenu = new JMenu("Archivo");
		archivoMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		menu.add(archivoMenu);
		
		guardarMenu = new JMenuItem("Guardar estado");
		guardarMenu.setIcon(new ImageIcon(MainForm.class.getResource("/javax/swing/plaf/metal/icons/ocean/floppy.gif")));
		guardarMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		archivoMenu.add(guardarMenu);
		
		cargarMenu = new JMenuItem("Cargar estado");
		cargarMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cargarMenu.setIcon(new ImageIcon(MainForm.class.getResource("/javax/swing/plaf/metal/icons/ocean/hardDrive.gif")));
		archivoMenu.add(cargarMenu);
		
		salirMenu = new JMenuItem("Salir");
		salirMenu.setIcon(new ImageIcon(MainForm.class.getResource("/javax/swing/plaf/metal/icons/ocean/close.gif")));
		salirMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		archivoMenu.add(salirMenu);
		
		mapaMenu = new JMenu("Mapa");
		mapaMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		menu.add(mapaMenu);
		
		resetearMapaMenu = new JMenuItem("Resetear mapa");
		resetearMapaMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		resetearMapaMenu.setIcon(new ImageIcon(MainForm.class.getResource("/com/sun/java/swing/plaf/motif/icons/Warn.gif")));
		mapaMenu.add(resetearMapaMenu);
		
		quitarRutasMenu = new JMenuItem("Quitar Rutas");
		quitarRutasMenu.setIcon(new ImageIcon(MainForm.class.getResource("/com/sun/javafx/scene/web/skin/Undo_16x16_JFX.png")));
		quitarRutasMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		mapaMenu.add(quitarRutasMenu);
		
		ciudadesMenu = new JMenu("Ciudades");
		ciudadesMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		menu.add(ciudadesMenu);
		
		agregarCiudadMenu = new JMenuItem("Agregar ciudad...");
		agregarCiudadMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		agregarCiudadMenu.setIcon(new ImageIcon(MainForm.class.getResource("/javax/swing/plaf/metal/icons/ocean/minimize.gif")));
		ciudadesMenu.add(agregarCiudadMenu);
		
		removerCiudadMenu = new JMenuItem("Remover ciudad...");
		removerCiudadMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		removerCiudadMenu.setIcon(new ImageIcon(MainForm.class.getResource("/javax/swing/plaf/metal/icons/ocean/maximize.gif")));
		ciudadesMenu.add(removerCiudadMenu);
		
		rutasMenu = new JMenu("Rutas");
		rutasMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		menu.add(rutasMenu);
		
		agregarRutaMenu = new JMenuItem("Agregar ruta...");
		agregarRutaMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		agregarRutaMenu.setIcon(new ImageIcon(MainForm.class.getResource("/javax/swing/plaf/metal/icons/ocean/collapsed.gif")));
		rutasMenu.add(agregarRutaMenu);
		
		removerRutaMenu = new JMenuItem("Remover ruta...");
		removerRutaMenu.setIcon(new ImageIcon(MainForm.class.getResource("/javax/swing/plaf/metal/icons/ocean/collapsed-rtl.gif")));
		removerRutaMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rutasMenu.add(removerRutaMenu);
		
		caminoMinimo = new JMenuItem("Buscar Camino Minimo...");
		caminoMinimo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		caminoMinimo.setIcon(new ImageIcon(MainForm.class.getResource("/com/sun/java/swing/plaf/motif/icons/Question.gif")));
		rutasMenu.add(caminoMinimo);
		
		consultasMenu = new JMenu("Consultas");
		consultasMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		menu.add(consultasMenu);
		
		infoCiudadesMenu = new JMenuItem("Info. ciudades...");
		infoCiudadesMenu.setIcon(new ImageIcon(MainForm.class.getResource("/com/sun/javafx/scene/control/skin/modena/HTMLEditor-Numbered.png")));
		infoCiudadesMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		consultasMenu.add(infoCiudadesMenu);
		
		infoRutasMenu = new JMenuItem("Info. rutas...");
		infoRutasMenu.setIcon(new ImageIcon(MainForm.class.getResource("/com/sun/javafx/scene/control/skin/modena/HTMLEditor-Numbered-Black.png")));
		infoRutasMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		consultasMenu.add(infoRutasMenu);
		
		infoCaminosMinimosMenu = new JMenuItem("Info. caminos m\u00EDnimos...");
		infoCaminosMinimosMenu.setIcon(new ImageIcon(MainForm.class.getResource("/com/sun/javafx/scene/web/skin/OrderedListNumbers_16x16_JFX.png")));
		infoCaminosMinimosMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		consultasMenu.add(infoCaminosMinimosMenu);
		
		ayudaMenu = new JMenu("Ayuda");
		ayudaMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		menu.add(ayudaMenu);
		
		cargarEjemploMenu = new JMenuItem("Cargar ejemplo");
		cargarEjemploMenu.setIcon(new ImageIcon(MainForm.class.getResource("/org/openstreetmap/gui/jmapviewer/images/plus.png")));
		cargarEjemploMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		ayudaMenu.add(cargarEjemploMenu);
		
		informacionMenu = new JMenuItem("Informaci\u00F3n...");
		informacionMenu.setIcon(new ImageIcon(MainForm.class.getResource("/com/sun/java/swing/plaf/motif/icons/Inform.gif")));
		informacionMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		ayudaMenu.add(informacionMenu);
		
		acercaDeMenu = new JMenuItem("Acerca de...");
		acercaDeMenu.setIcon(new ImageIcon(MainForm.class.getResource("/javax/swing/plaf/basic/icons/JavaCup16.png")));
		acercaDeMenu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		ayudaMenu.add(acercaDeMenu);
		
		setMenuListeners();
	}

	private void setMenuListeners() 
	{
		guardarMenu.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	FileManager.guardarMapa(grafoMapa);
            	String linea1 = ("           �Mapa guardado!\r\n");
    			JOptionPane.showMessageDialog(null, linea1);
            }
        });
		
		cargarMenu.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	try 
            	{
            		grafoMapa = FileManager.cargarMapa();
                	Graficador.resetearCiudades(grafoMapa.getCiudades(), mapaPantalla);
                	Graficador.resetearRutas(grafoMapa.getRutas(), mapaPantalla);
            	}catch (Exception ex)
            	{
            		inicializarGrafo();
            		String linea1 = ("      No existe mapa precargado!\r\n");
        			JOptionPane.showMessageDialog(null, linea1);
            	}
            }
        });
		
		salirMenu.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	System.exit(0);
            }
        });
		
		resetearMapaMenu.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	grafoMapa.resetearMapa(true);
            	Graficador.resetearCiudades(grafoMapa.getCiudades(), mapaPantalla);
            	Graficador.resetearRutas(grafoMapa.getRutas(), mapaPantalla);
            }
        });
		
		quitarRutasMenu.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	grafoMapa.resetearMapa(false);
            	Graficador.resetearRutas(grafoMapa.getRutas(), mapaPantalla);
            }
        });
		
		removerCiudadMenu.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	if (grafoMapa.getCiudades().size() >= 1)
            	{
                	ventanaRemoveCiudad = new VentanaRemoveCiudad(grafoMapa, mapaPantalla);
                	ventanaRemoveCiudad.setVisible(true);
                	ventanaRemoveCiudad.setLocationRelativeTo(null);
            	}
            	else
            	{
        			String linea1 = ("No se ha ingresado el n�mero de ciudades suficientes.\r\n");
        			JOptionPane.showMessageDialog(null, linea1);
            	}
            }
        });
		
		agregarCiudadMenu.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	ventanaAgregarCiudadManual = new VentanaAddCiudadManual(mapaPantalla, grafoMapa);
            	ventanaAgregarCiudadManual.setVisible(true);
            	ventanaAgregarCiudadManual.setLocationRelativeTo(null);
            }
        });
		
		agregarRutaMenu.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	if (grafoMapa.getCiudades().size() >= 2)
            	{
	            	ventanaAgregarRuta = new VentanaAddRuta(grafoMapa, mapaPantalla);
	            	ventanaAgregarRuta.setVisible(true);
	            	ventanaAgregarRuta.setLocationRelativeTo(null);
            	}
            	else
            	{
        			String linea1 = ("No se ha ingresado el n�mero de ciudades suficientes.\r\n");
        			JOptionPane.showMessageDialog(null, linea1);
            	}
            }
        });
		
		removerRutaMenu.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	if (grafoMapa.getRutas().size() >= 1)
            	{
            		ventanaRemoveRuta = new VentanaRemoveRuta(grafoMapa, mapaPantalla);
            		ventanaRemoveRuta.setVisible(true);
            		ventanaRemoveRuta.setLocationRelativeTo(null);
            	}
            	else
            	{
        			String linea1 = ("No se ha ingresado el n�mero de rutas suficientes.\r\n");
        			JOptionPane.showMessageDialog(null, linea1);
            	}
            }
        });
		
		caminoMinimo.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	if (grafoMapa.getCiudades().size() >= 2 && grafoMapa.getRutas().size() >= 1)
            	{
            		ventanaCaminoMinimo = new VentanaCaminoMinimo(grafoMapa, mapaPantalla);
                	ventanaCaminoMinimo.setVisible(true);
                	ventanaCaminoMinimo.setLocationRelativeTo(null);
            	}
            	else
            	{
            		String linea1 = ("No se ha ingresado el n�mero de ciudades/rutas suficientes.\r\n");
        			JOptionPane.showMessageDialog(null, linea1);
            	}
            }
        });
		
		infoCiudadesMenu.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	ventanaInfoCiudad = new VentanaInfoCiudades(grafoMapa, mapaPantalla);
            	ventanaInfoCiudad.setVisible(true);
            	ventanaInfoCiudad.setLocationRelativeTo(null);
            }
        });
		
		infoRutasMenu.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	ventanaInfoRutas = new VentanaInfoRutas(grafoMapa, mapaPantalla);
            	ventanaInfoRutas.setVisible(true);
            	ventanaInfoRutas.setLocationRelativeTo(null);
            }
        });
		
		infoCaminosMinimosMenu.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	ventanaInfoCaminosMinimos = new VentanaInfoCaminosMinimos(grafoMapa, mapaPantalla);
            	ventanaInfoCaminosMinimos.setVisible(true);
            	ventanaInfoCaminosMinimos.setLocationRelativeTo(null);
            }
        });
		
		cargarEjemploMenu.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	try 
            	{
            		grafoMapa = FileManager.cargarMapaEjemplo();
                	Graficador.resetearCiudades(grafoMapa.getCiudades(), mapaPantalla);
                	Graficador.resetearRutas(grafoMapa.getRutas(), mapaPantalla);
            	}catch (Exception ex)
            	{
            		String linea1 = ("      No existe mapa precargado!\r\n");
        			JOptionPane.showMessageDialog(null, linea1);
            	}
            }
        });
		
		informacionMenu.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	ventanaAyuda = new VentanaAyuda();
            	ventanaAyuda.setVisible(true);
            	ventanaAyuda.setLocationRelativeTo(null);
            }
        });
		
		acercaDeMenu.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
            	String nl = System.getProperty("line.separator");
        		String linea1 = ("Aplicaci�n realizada por Leandro Yasutake para Programaci�n III\r\n");
        		String linea2 = ("                   Universidad Nacional de General Sarmiento\r\n");
        		JOptionPane.showMessageDialog(null, linea1+nl+linea2+nl);
            }
        });
	}

	private void inicializarMapa() 
	{
		mapaPantalla = new JMapViewer();
		mapaPantalla.setZoomContolsVisible(false);
		mapaPantalla.setDisplayPositionByLatLon(-40, -64.5, 4);
		getContentPane().add(mapaPantalla);
		mapaPantalla.setZoomContolsVisible(true);
		
		mapaPantalla.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
        		ventanaAgregarCiudad = new VentanaAddCiudad(mapaPantalla, grafoMapa, mapaPantalla.getPosition(e.getPoint()));
        		ventanaAgregarCiudad.setVisible(true);
        		ventanaAgregarCiudad.setLocationRelativeTo(null);
            }
        });
	}
}
