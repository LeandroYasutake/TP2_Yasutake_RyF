package GUI;

import java.awt.Cursor;
import java.util.List;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import Mapa.Ciudad;
import Mapa.Mapa;
import Mapa.Trayecto;

public class VentanaCaminoMinimoAlgoritmo extends SwingWorker<Trayecto, Integer>
{
	private Mapa mapaGrafo;
	private Ciudad origen;
	private Ciudad destino;
	private int maxPeajes;
	private VentanaCaminoMinimo ventanaCaminoMinimo;
	
	public VentanaCaminoMinimoAlgoritmo(Mapa mapaGrafo, Ciudad origen, Ciudad destino, 
			int maximosPeajes, VentanaCaminoMinimo ventanaCaminoMinimo)
	{
		this.mapaGrafo = mapaGrafo;
		this.origen = origen;
		this.destino = destino;
		this.maxPeajes = maximosPeajes;
		this.ventanaCaminoMinimo = ventanaCaminoMinimo;
	}
	
	@Override
	protected Trayecto doInBackground() throws Exception 
	{
		ventanaCaminoMinimo.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		int posOrigen = this.mapaGrafo.getPosCiudad(this.origen);
		int posDestino = this.mapaGrafo.getPosCiudad(this.destino);
		Random random = new Random();
		Trayecto camMinimo = this.mapaGrafo.caminoMinimo(posOrigen, posDestino, this.maxPeajes);
		
		for (int i = 0; i < 100; i++) 
		{	
			try 
			{
				Thread.sleep(random.nextInt(25));
			} catch (InterruptedException e) 
			{
				String linea1 = ("    B�squeda interrumpida!\r\n");
				JOptionPane.showMessageDialog(null, linea1);
			}
			publish(i+1);
		}
		return camMinimo;
	}
	
	@Override
	protected void process(List<Integer> chunks) 
	{
		ventanaCaminoMinimo.getProgressBar().setValue(chunks.get(0));
	}
	
	@Override
	protected void done() 
	{
		ventanaCaminoMinimo.setCursor(null);
		ventanaCaminoMinimo.getProgressBar().setValue(100);
	}
}
