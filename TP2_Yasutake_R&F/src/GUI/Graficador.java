package GUI;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JComboBox;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import Mapa.Ciudad;
import Mapa.Mapa;
import Mapa.Ruta;
import Mapa.Trayecto;

public class Graficador 
{
	public static void dibujarRuta(Ruta ruta, JMapViewer mapaPantalla, boolean caminoMinimo)
    {
        ArrayList<Coordinate> listaCoor = new ArrayList<Coordinate>();

        listaCoor.add(new Coordinate(ruta.getOrigen().getLatitud(), ruta.getOrigen().getLongitud()));
        listaCoor.add(new Coordinate(ruta.getOrigen().getLatitud() + 0.00001, ruta.getOrigen().getLongitud() + 0.00001));
        listaCoor.add(new Coordinate(ruta.getDestino().getLatitud(), ruta.getDestino().getLongitud()));

        MapPolygon poligonoRecta = new MapPolygonImpl(listaCoor);
        
        if (!caminoMinimo)
        {
        	if (ruta.getPeaje() == 1)
        		poligonoRecta.getStyle().setColor(Color.RED);
	        else
	        	poligonoRecta.getStyle().setColor(Color.BLUE);
        }
        else 
        	poligonoRecta.getStyle().setColor(Color.CYAN);
        
        mapaPantalla.addMapPolygon(poligonoRecta);
    }
	
	public static void resetearRutas(ArrayList<Ruta> rutas, JMapViewer mapaPantalla)
	{
		mapaPantalla.removeAllMapPolygons();
		for (Ruta ruta: rutas)
			dibujarRuta(ruta, mapaPantalla, false);
	}
	
	public static void resetearCiudades(ArrayList<Ciudad> ciudades, JMapViewer mapaPantalla)
	{
		mapaPantalla.removeAllMapMarkers();
		
		for (Ciudad ciudad : ciudades)
			agregarMarcador(ciudad, mapaPantalla, Color.BLUE);
	}
	
	public static void agregarMarcador(Ciudad ciudad, JMapViewer mapaPantalla, Color color)
	{
		Coordinate c = new Coordinate(ciudad.getLatitud(), ciudad.getLongitud());
		MapMarker marker = new MapMarkerDot(ciudad.getNombre().toUpperCase(), c);
		marker.getStyle().setBackColor(color);
		marker.getStyle().setColor(color);
		mapaPantalla.addMapMarker(marker);
	}
	
	public static void dibujarTrayectoMinimo(Trayecto minimo, JMapViewer mapaPantalla)
	{
		for (Ruta subTrayecto : minimo.getCaminoMinimo())
			Graficador.dibujarRuta(subTrayecto, mapaPantalla, true);
		
		for (Ciudad ciudad : minimo.getCiudadesVisitadas())
			Graficador.agregarMarcador(ciudad, mapaPantalla, Color.YELLOW);
	}
	
	public static void dibujarParCiudades(Ruta ruta, JMapViewer mapaPantalla)
	{
		agregarMarcador(ruta.getOrigen(), mapaPantalla, Color.YELLOW);
		agregarMarcador(ruta.getDestino(), mapaPantalla, Color.YELLOW);
	}
	
	public static void dibujarVecinos(ArrayList<Ciudad> vecinos, JMapViewer mapaPantalla)
	{
		for (Ciudad vecino : vecinos)
			 agregarMarcador(vecino, mapaPantalla, Color.GREEN);
	}
	
	public static JComboBox<Ciudad> llenarComboBoxCiudades(JComboBox<Ciudad> comboBox, Mapa mapaGrafo)
	{
		Collections.sort(mapaGrafo.getCiudades(), FactoryComparator.getComparadorCiudades());
		
		for (Ciudad ciudad : mapaGrafo.getCiudades())
			comboBox.addItem(ciudad);
		
		return comboBox;
	}
	
	public static JComboBox<Ruta> llenarComboBoxRutas(JComboBox<Ruta> comboBox, Mapa mapaGrafo)
	{
		Collections.sort(mapaGrafo.getRutas(), FactoryComparator.getComparadorRutas());
		
		for (Ruta Ruta : mapaGrafo.getRutas())
			comboBox.addItem(Ruta);
		
		return comboBox;
	}
	
	public static JComboBox<Trayecto> llenarComboBoxTrayectos(JComboBox<Trayecto> comboBox, Mapa mapaGrafo)
	{
		Collections.sort(mapaGrafo.getTrayectos(), FactoryComparator.getComparadorTrayectos());
		
		for (Trayecto subTrayecto : mapaGrafo.getTrayectos())
			comboBox.addItem(subTrayecto);
		
		return comboBox;
	}
}
