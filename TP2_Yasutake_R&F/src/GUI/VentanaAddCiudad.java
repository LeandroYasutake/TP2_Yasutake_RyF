package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;

import Mapa.Ciudad;
import Mapa.Mapa;

import java.awt.Font;

import javax.swing.SwingConstants;

public class VentanaAddCiudad extends JDialog 
{

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JButton botonOk;
	private JButton botonCancelar;
	private JTextField textNombre;
	private JLabel texto1;
	private JLabel nombre;

	public VentanaAddCiudad(JMapViewer mapaPantalla, Mapa mapa, Coordinate coordenada) 
	{
		setResizable(false);
		setBounds(100, 100, 475, 215);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		setLabels();
		setText();
		setBotones(mapaPantalla, mapa, coordenada);
	}
	
	private void setLabels()
	{
		texto1 = new JLabel("Por favor, ingrese un nombre para la nueva ciudad:");
		texto1.setFont(new Font("Tahoma", Font.BOLD, 13));
		texto1.setBounds(67, 11, 334, 14);
		contentPanel.add(texto1);
		
		nombre = new JLabel("Nombre: ");
		nombre.setFont(new Font("Tahoma", Font.PLAIN, 12));
		nombre.setBounds(10, 79, 51, 14);
		contentPanel.add(nombre);
	}
	
	private void setText()
	{
		textNombre = new JTextField();
		textNombre.setFont(new Font("Tahoma", Font.PLAIN, 12));
		textNombre.setHorizontalAlignment(SwingConstants.CENTER);
		textNombre.setBounds(78, 76, 381, 20);
		contentPanel.add(textNombre);
		textNombre.setColumns(10);
	}
	
	private void setBotones(JMapViewer mapaPantalla, Mapa mapa, Coordinate coordenada) 
	{
		botonOk = new JButton("Aceptar");
		botonOk.setFont(new Font("Tahoma", Font.PLAIN, 12));
		botonOk.setBounds(67, 153, 133, 23);
		contentPanel.add(botonOk);
		botonOk.setActionCommand("OK");
		getRootPane().setDefaultButton(botonOk);
		botonOk.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (!textNombre.getText().isEmpty())
				{
					Ciudad ciudad = new Ciudad(textNombre.getText(), coordenada.getLat(), coordenada.getLon());
					
					if (!mapa.getCiudades().contains(ciudad))
					{
						mapa.addCiudad(ciudad);
						Graficador.agregarMarcador(ciudad, mapaPantalla, Color.BLUE);
						String linea1 = ("           Ciudad agregada!\r\n");
	        			JOptionPane.showMessageDialog(null, linea1);
						dispose();
					}
					
					else
					{
						String linea1 = ("  La ciudad ingresada ya existe, por favor ingrese otra.\r\n");
						JOptionPane.showMessageDialog(null, linea1);
					}
				}
				
				else
				{
					String linea1 = ("Por favor, ingrese un nombre correcto para la ciudad deseada.\r\n");
					JOptionPane.showMessageDialog(null, linea1);
				}
			}
		});
		
		botonCancelar = new JButton("Cancelar");
		botonCancelar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		botonCancelar.setBounds(267, 153, 133, 23);
		contentPanel.add(botonCancelar);
		botonCancelar.setActionCommand("Cancel");
		botonCancelar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
	}
}
