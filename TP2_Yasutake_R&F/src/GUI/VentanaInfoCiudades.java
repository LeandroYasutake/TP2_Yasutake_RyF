package GUI;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.openstreetmap.gui.jmapviewer.JMapViewer;

import Mapa.Ciudad;
import Mapa.Mapa;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JButton;

public class VentanaInfoCiudades extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JLabel titulo;
	private JLabel cantCiudades;
	private JLabel n�ciudades;
	private JLabel ciudad;
	private JLabel cantVecinos;
	private JLabel n�vecinos;
	private JLabel vecinos;
	private JComboBox<Ciudad> comboBoxCiudades;
	private JTextArea ciudadesVecinas;
	private JButton botonSalir;
	private JScrollPane scroll;

	public VentanaInfoCiudades(Mapa mapaGrafo, JMapViewer mapaPantalla) 
	{
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setResizable(false);
		setBounds(100, 100, 396, 366);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		setLabels();
		setText();
		setComboBox(mapaGrafo, mapaPantalla);
		setCantidades(mapaGrafo);
		setBotones(mapaGrafo, mapaPantalla);
	}

	private void setLabels()
	{
		titulo = new JLabel("Informaci\u00F3n sobre Ciudades:");
		titulo.setFont(new Font("Tahoma", Font.BOLD, 13));
		titulo.setBounds(102, 11, 185, 14);
		setTitle("Informaci�n: ciudades");
		contentPanel.add(titulo);
		
		cantCiudades = new JLabel("Cantidad de Ciudades:");
		cantCiudades.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cantCiudades.setBounds(10, 40, 121, 14);
		contentPanel.add(cantCiudades);
		
		n�ciudades = new JLabel("0");
		n�ciudades.setFont(new Font("Tahoma", Font.PLAIN, 12));
		n�ciudades.setHorizontalAlignment(SwingConstants.CENTER);
		n�ciudades.setBounds(333, 40, 46, 14);
		contentPanel.add(n�ciudades);
		
		ciudad = new JLabel("Ciudad:");
		ciudad.setFont(new Font("Tahoma", Font.PLAIN, 12));
		ciudad.setBounds(10, 83, 46, 14);
		contentPanel.add(ciudad);
		
		cantVecinos = new JLabel("N\u00BA Vecinos:");
		cantVecinos.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cantVecinos.setBounds(10, 130, 68, 14);
		contentPanel.add(cantVecinos);
		
		n�vecinos = new JLabel("0");
		n�vecinos.setHorizontalAlignment(SwingConstants.CENTER);
		n�vecinos.setFont(new Font("Tahoma", Font.PLAIN, 12));
		n�vecinos.setBounds(333, 130, 46, 14);
		contentPanel.add(n�vecinos);
		
		vecinos = new JLabel("Vecinos:");
		vecinos.setFont(new Font("Tahoma", Font.PLAIN, 12));
		vecinos.setBounds(10, 166, 53, 14);
		contentPanel.add(vecinos);
	}
	
	private void setText() 
	{
		ciudadesVecinas = new JTextArea();
		ciudadesVecinas.setLineWrap(true);
		ciudadesVecinas.setEditable(false);
		ciudadesVecinas.setBounds(60, 166, 319, 101);
		contentPanel.add(ciudadesVecinas);
		
		scroll = new JScrollPane (ciudadesVecinas);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroll.setBounds(60, 166, 319, 127);
		contentPanel.add(scroll);
	}
	
	private void setComboBox(Mapa mapaGrafo, JMapViewer mapaPantalla) 
	{
		comboBoxCiudades = new JComboBox<Ciudad>();
		comboBoxCiudades = Graficador.llenarComboBoxCiudades(comboBoxCiudades, mapaGrafo);
		comboBoxCiudades.setBounds(185, 75, 194, 22);
		comboBoxCiudades.setSelectedIndex(-1);
		comboBoxCiudades.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				Ciudad ciudad = (Ciudad) comboBoxCiudades.getSelectedItem();
				setDatosCiudad(ciudad, mapaGrafo);
				Graficador.dibujarVecinos(mapaGrafo.getVecinosArray(ciudad), mapaPantalla);
				Graficador.agregarMarcador(ciudad, mapaPantalla, Color.YELLOW);
			}
		});
		contentPanel.add(comboBoxCiudades);
		
	}
	
	protected void setDatosCiudad(Ciudad ciudad, Mapa mapaGrafo) 
	{
		setVecinos(mapaGrafo);
		n�vecinos.setText(String.valueOf(mapaGrafo.numeroVecinos(ciudad)));
	}	
	
	private void setCantidades(Mapa mapaGrafo) 
	{
		n�ciudades.setText(String.valueOf(mapaGrafo.getCiudades().size()));
	}
	
	private void setVecinos(Mapa mapaGrafo) 
	{
		ciudadesVecinas.setText(mapaGrafo.getVecinos((Ciudad)comboBoxCiudades.getSelectedItem()));
	}
	
	private void setBotones(Mapa mapaGrafo, JMapViewer mapaPantalla) 
	{
		botonSalir = new JButton("Salir");
		botonSalir.setFont(new Font("Tahoma", Font.PLAIN, 12));
		botonSalir.setBounds(149, 304, 91, 23);
		botonSalir.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				dispose();
			}
		});
		contentPanel.add(botonSalir);
	}
}	
