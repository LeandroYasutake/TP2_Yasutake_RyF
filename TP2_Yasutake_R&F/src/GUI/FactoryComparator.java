package GUI;

import java.util.Comparator;

import Mapa.Ciudad;
import Mapa.Ruta;
import Mapa.Trayecto;

public class FactoryComparator 
{
	public static Comparator<Ciudad> getComparadorCiudades()
	{
		return (new Comparator<Ciudad>()
		{
			public int compare(Ciudad c1, Ciudad c2)
			{
				return c1.getNombre().compareTo(c2.getNombre());
			}
		});
	}
	
	public static Comparator<Ruta> getComparadorRutas()
	{
		return (new Comparator<Ruta>()
		{
			public int compare(Ruta r1, Ruta r2)
			{
				return r1.getOrigen().getNombre().compareTo(r2.getOrigen().getNombre());
			}
		});
	}
	
	public static Comparator<Trayecto> getComparadorTrayectos()
	{
		return (new Comparator<Trayecto>()
		{
			public int compare(Trayecto t1, Trayecto t2)
			{
				return t1.toString().compareTo(t2.toString());
			}
		});
	}
}	
