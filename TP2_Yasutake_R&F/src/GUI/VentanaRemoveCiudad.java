package GUI;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import org.openstreetmap.gui.jmapviewer.JMapViewer;

import Mapa.Ciudad;
import Mapa.Mapa;

public class VentanaRemoveCiudad extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JLabel titulo;
	private JLabel ciudad;
	private JComboBox<Ciudad> ciudadesCombo;
	private JButton botonRemover;
	private JButton botonCancelar;
	
	public VentanaRemoveCiudad(Mapa mapaGrafo, JMapViewer mapaPantalla) 
	{
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setResizable(false);
		setBounds(100, 100, 303, 210);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		setLabels();
		setComboBox(mapaGrafo, mapaPantalla);
		setBotones(mapaGrafo, mapaPantalla);
	}
	
	private void setLabels()
	{
		titulo = new JLabel("Aqui puede remover la ciudad deseada:");
		titulo.setFont(new Font("Tahoma", Font.BOLD, 13));
		titulo.setBounds(20, 11, 257, 16);
		contentPanel.add(titulo);
		
		ciudad = new JLabel("Ciudad:");
		ciudad.setFont(new Font("Tahoma", Font.PLAIN, 12));
		ciudad.setBounds(15, 71, 46, 16);
		contentPanel.add(ciudad);
	}
	
	private void setComboBox(Mapa mapaGrafo, JMapViewer mapaPantalla)
	{
		ciudadesCombo = new JComboBox<Ciudad>();
		ciudadesCombo = Graficador.llenarComboBoxCiudades(ciudadesCombo, mapaGrafo);
		ciudadesCombo.setBounds(87, 70, 190, 20);
		ciudadesCombo.setSelectedIndex(-1);
		ciudadesCombo.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				Ciudad ciudad = (Ciudad) ciudadesCombo.getSelectedItem();
				Graficador.agregarMarcador(ciudad, mapaPantalla, Color.YELLOW);
			}
		});
		contentPanel.add(ciudadesCombo);
	}
	
	private void setBotones(Mapa mapaGrafo, JMapViewer mapaPantalla)
	{
		botonCancelar = new JButton("Cancelar");
		botonCancelar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		botonCancelar.setBounds(188, 136, 89, 23);
		botonCancelar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				dispose();
			}
		});
		contentPanel.add(botonCancelar);
		
		botonRemover = new JButton("Remover ");
		botonRemover.setFont(new Font("Tahoma", Font.PLAIN, 12));
		botonRemover.setBounds(15, 136, 89, 23);
		botonRemover.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Ciudad ciudad = (Ciudad)ciudadesCombo.getSelectedItem();
				mapaGrafo.removeCiudad(ciudad);
				Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				Graficador.resetearRutas(mapaGrafo.getRutas(), mapaPantalla);
				String linea1 = ("    Ciudad removida!\r\n");
    			JOptionPane.showMessageDialog(null, linea1);
    			Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				dispose();
			}
		});
		contentPanel.add(botonCancelar);
		contentPanel.add(botonRemover);
	}
}
