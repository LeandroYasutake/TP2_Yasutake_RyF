package GUI;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Mapa.Mapa;
import Mapa.Ruta;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import javax.swing.JButton;

import org.openstreetmap.gui.jmapviewer.JMapViewer;

public class VentanaInfoRutas extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JLabel titulo;
	private JLabel cantRutas;
	private JLabel n�Rutas;
	private JLabel cantRutasSP;
	private JLabel n�RutasSP;
	private JLabel cantRutasP;
	private JLabel n�RutasCP;
	private JLabel ruta;
	private JComboBox<Ruta> comboBoxRutas;
	private JLabel ciudadOrigen;
	private JLabel ciudadDestino;
	private JLabel labelCiudadOrigen;
	private JLabel labelCiudadDestino;
	private JLabel labelDistanciaTotal;
	private JLabel labelPeaje;
	private JLabel n�DistanciaTotal;
	private JLabel hayPeaje;
	private JButton botonSalir;
	
	public VentanaInfoRutas(Mapa mapaGrafo, JMapViewer mapaPantalla) 
	{
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setResizable(false);
		setBounds(100, 100, 450, 381);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		setTitle("Informaci�n: rutas");
		contentPanel.setLayout(null);
		
		setLabels();
		setComboBox(mapaGrafo, mapaPantalla);
		setInformacion(mapaGrafo);
		setBotones(mapaGrafo, mapaPantalla);
	}
	
	private void setLabels() 
	{
		titulo = new JLabel("Informaci\u00F3n sobre rutas:");
		titulo.setFont(new Font("Tahoma", Font.BOLD, 13));
		titulo.setBounds(133, 11, 168, 14);
		contentPanel.add(titulo);
		
		cantRutas = new JLabel("Cantidad de Rutas:");
		cantRutas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cantRutas.setBounds(10, 54, 104, 14);
		contentPanel.add(cantRutas);
		
		n�Rutas = new JLabel("0");
		n�Rutas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		n�Rutas.setHorizontalAlignment(SwingConstants.RIGHT);
		n�Rutas.setBounds(388, 55, 46, 14);
		contentPanel.add(n�Rutas);
		
		cantRutasSP = new JLabel("Rutas sin Peajes:");
		cantRutasSP.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cantRutasSP.setBounds(10, 79, 104, 14);
		contentPanel.add(cantRutasSP);
		
		n�RutasSP = new JLabel("0");
		n�RutasSP.setHorizontalAlignment(SwingConstants.RIGHT);
		n�RutasSP.setFont(new Font("Tahoma", Font.PLAIN, 12));
		n�RutasSP.setBounds(388, 80, 46, 14);
		contentPanel.add(n�RutasSP);
		
		cantRutasP = new JLabel("Rutas con Peajes:");
		cantRutasP.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cantRutasP.setBounds(10, 104, 104, 14);
		contentPanel.add(cantRutasP);
		
		n�RutasCP = new JLabel("0");
		n�RutasCP.setFont(new Font("Tahoma", Font.PLAIN, 12));
		n�RutasCP.setHorizontalAlignment(SwingConstants.RIGHT);
		n�RutasCP.setBounds(388, 105, 46, 14);
		contentPanel.add(n�RutasCP);
	
		ruta = new JLabel("Ruta:");
		ruta.setFont(new Font("Tahoma", Font.PLAIN, 12));
		ruta.setBounds(10, 155, 46, 14);
		contentPanel.add(ruta);
		
		ciudadOrigen = new JLabel("Ciudad Origen:");
		ciudadOrigen.setFont(new Font("Tahoma", Font.PLAIN, 12));
		ciudadOrigen.setBounds(10, 191, 86, 14);
		contentPanel.add(ciudadOrigen);
		
		labelCiudadOrigen = new JLabel("-");
		labelCiudadOrigen.setHorizontalAlignment(SwingConstants.RIGHT);
		labelCiudadOrigen.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelCiudadOrigen.setBounds(187, 192, 247, 14);
		contentPanel.add(labelCiudadOrigen);
		
		ciudadDestino = new JLabel("Ciudad Destino:");
		ciudadDestino.setFont(new Font("Tahoma", Font.PLAIN, 12));
		ciudadDestino.setBounds(10, 216, 86, 14);
		contentPanel.add(ciudadDestino);
		
		labelCiudadDestino = new JLabel("-");
		labelCiudadDestino.setHorizontalAlignment(SwingConstants.RIGHT);
		labelCiudadDestino.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelCiudadDestino.setBounds(187, 217, 247, 14);
		contentPanel.add(labelCiudadDestino);
		
		labelDistanciaTotal = new JLabel("Distancia Total:");
		labelDistanciaTotal.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelDistanciaTotal.setBounds(10, 241, 86, 14);
		contentPanel.add(labelDistanciaTotal);
		
		n�DistanciaTotal = new JLabel("0");
		n�DistanciaTotal.setHorizontalAlignment(SwingConstants.RIGHT);
		n�DistanciaTotal.setFont(new Font("Tahoma", Font.PLAIN, 12));
		n�DistanciaTotal.setBounds(388, 242, 46, 14);
		contentPanel.add(n�DistanciaTotal);
		
		labelPeaje = new JLabel("Peaje:");
		labelPeaje.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelPeaje.setBounds(10, 267, 86, 14);
		contentPanel.add(labelPeaje);
		
		hayPeaje = new JLabel("-");
		hayPeaje.setHorizontalAlignment(SwingConstants.RIGHT);
		hayPeaje.setFont(new Font("Tahoma", Font.PLAIN, 12));
		hayPeaje.setBounds(388, 268, 46, 14);
		contentPanel.add(hayPeaje);
	}
	
	private void setComboBox(Mapa mapaGrafo, JMapViewer mapaPantalla) 
	{
		comboBoxRutas = new JComboBox<Ruta>();
		comboBoxRutas = Graficador.llenarComboBoxRutas(comboBoxRutas, mapaGrafo);
		comboBoxRutas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		comboBoxRutas.setBounds(136, 153, 298, 20);
		comboBoxRutas.setSelectedIndex(-1);
		comboBoxRutas.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Graficador.resetearRutas(mapaGrafo.getRutas(), mapaPantalla);
				Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				Ruta ruta = (Ruta) comboBoxRutas.getSelectedItem();
				setDatosRuta(ruta);
				Graficador.dibujarRuta(ruta, mapaPantalla, true);
				Graficador.dibujarParCiudades(ruta, mapaPantalla);
			}
		});
		contentPanel.add(comboBoxRutas);
	}
	
	private void setDatosRuta(Ruta ruta) 
	{
		labelCiudadOrigen.setText(ruta.getOrigen().getNombre());
		labelCiudadDestino.setText(ruta.getDestino().getNombre());
		n�DistanciaTotal.setText(String.valueOf(ruta.getDistancia()));
		if (ruta.getPeaje() == 1)
			hayPeaje.setText("Si");
		else
			hayPeaje.setText("No");
	}
	
	private void setInformacion(Mapa mapaGrafo) 
	{
		n�Rutas.setText(String.valueOf(mapaGrafo.getRutas().size()));
		n�RutasSP.setText(String.valueOf(mapaGrafo.getRutasSP()));
		n�RutasCP.setText(String.valueOf(mapaGrafo.getRutasCP()));
	}
	
	private void setBotones(Mapa mapaGrafo, JMapViewer mapaPantalla) 
	{
		botonSalir = new JButton("Salir");
		botonSalir.setFont(new Font("Tahoma", Font.PLAIN, 12));
		botonSalir.setBounds(177, 320, 89, 23);
		botonSalir.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				Graficador.resetearRutas(mapaGrafo.getRutas(), mapaPantalla);
				dispose();
			}
		});
		contentPanel.add(botonSalir);
	}
}
