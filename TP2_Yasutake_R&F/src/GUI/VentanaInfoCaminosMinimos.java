package GUI;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import Mapa.Mapa;
import Mapa.Trayecto;

import javax.swing.SwingConstants;
import javax.swing.JTextArea;

import org.openstreetmap.gui.jmapviewer.JMapViewer;

public class VentanaInfoCaminosMinimos extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JComboBox<Trayecto> comboBoxCM;
	private JButton botonSalir;
	private JLabel titulo;
	private JLabel labelBusquedasRealizadas;
	private JLabel n�BusquedasRealizadas;
	private JLabel labelCaminosMin;
	private JLabel labelRutasUtilizadas;
	private JLabel n�RutasUtilizadas;
	private JLabel labelCantidadPeajes;
	private JLabel n�CantPeajes;
	private JLabel labelDistanciaTotal;
	private JLabel n�DistanciaTotal;
	private JLabel labelCiudadesVisitadas;
	private JLabel n�CiudadesVisitadas;
	private JLabel labelRutas;
	private JLabel labelCiudades;
	private JTextArea textRutas;
	private JTextArea textCiudades;
	private JScrollPane scroll1;
	private JScrollPane scroll2;

	public VentanaInfoCaminosMinimos(Mapa mapaGrafo, JMapViewer mapaPantalla) 
	{
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 450, 645);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setTitle("Informaci�n: caminos m�nimos");
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		setLabels(mapaGrafo);
		setBotones(mapaGrafo, mapaPantalla);
		setComboBox(mapaGrafo, mapaPantalla);
		setTextAreas();
	}
	
	private void setLabels(Mapa mapaGrafo) 
	{
		contentPanel.setLayout(null);
		titulo = new JLabel("Aqu\u00ED puede consultar las b\u00FAsquedas realizadas:");
		titulo.setBounds(62, 11, 309, 14);
		titulo.setFont(new Font("Tahoma", Font.BOLD, 13));
		contentPanel.add(titulo);
	
		labelBusquedasRealizadas = new JLabel("B\u00FAsquedas realizadas:");
		labelBusquedasRealizadas.setBounds(10, 55, 123, 14);
		labelBusquedasRealizadas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPanel.add(labelBusquedasRealizadas);
	
		n�BusquedasRealizadas = new JLabel(String.valueOf(mapaGrafo.getTrayectos().size()));
		n�BusquedasRealizadas.setBounds(378, 56, 46, 14);
		n�BusquedasRealizadas.setHorizontalAlignment(SwingConstants.RIGHT);
		n�BusquedasRealizadas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPanel.add(n�BusquedasRealizadas);
		
		labelCaminosMin = new JLabel("Caminos M\u00EDnimos:");
		labelCaminosMin.setBounds(10, 94, 104, 14);
		labelCaminosMin.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPanel.add(labelCaminosMin);
		
		labelRutasUtilizadas = new JLabel("Rutas utilizadas:");
		labelRutasUtilizadas.setBounds(10, 198, 104, 14);
		labelRutasUtilizadas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPanel.add(labelRutasUtilizadas);
		
		n�RutasUtilizadas = new JLabel("0");
		n�RutasUtilizadas.setBounds(378, 198, 46, 14);
		n�RutasUtilizadas.setHorizontalAlignment(SwingConstants.RIGHT);
		n�RutasUtilizadas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPanel.add(n�RutasUtilizadas);
		
		labelCantidadPeajes = new JLabel("Cantidad de Peajes:");
		labelCantidadPeajes.setBounds(10, 173, 123, 14);
		labelCantidadPeajes.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPanel.add(labelCantidadPeajes);
		
		n�CantPeajes = new JLabel("0");
		n�CantPeajes.setBounds(378, 174, 46, 14);
		n�CantPeajes.setHorizontalAlignment(SwingConstants.RIGHT);
		n�CantPeajes.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPanel.add(n�CantPeajes);
		
		labelDistanciaTotal = new JLabel("Distancia Total:");
		labelDistanciaTotal.setBounds(10, 147, 89, 14);
		labelDistanciaTotal.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPanel.add(labelDistanciaTotal);
		
		n�DistanciaTotal = new JLabel("0");
		n�DistanciaTotal.setBounds(378, 148, 46, 14);
		n�DistanciaTotal.setFont(new Font("Tahoma", Font.PLAIN, 12));
		n�DistanciaTotal.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPanel.add(n�DistanciaTotal);
		
		labelCiudadesVisitadas = new JLabel("Ciudades visitadas:");
		labelCiudadesVisitadas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelCiudadesVisitadas.setBounds(10, 221, 104, 14);
		contentPanel.add(labelCiudadesVisitadas);
		
		n�CiudadesVisitadas = new JLabel("0");
		n�CiudadesVisitadas.setHorizontalAlignment(SwingConstants.RIGHT);
		n�CiudadesVisitadas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		n�CiudadesVisitadas.setBounds(378, 221, 46, 14);
		contentPanel.add(n�CiudadesVisitadas);
		
		labelRutas = new JLabel("Rutas:");
		labelRutas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelRutas.setBounds(10, 257, 34, 14);
		contentPanel.add(labelRutas);
	
		labelCiudades = new JLabel("Ciudades:");
		labelCiudades.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelCiudades.setBounds(10, 417, 52, 14);
		contentPanel.add(labelCiudades);
	}
	
	private void setBotones(Mapa mapaGrafo, JMapViewer mapaPantalla) 
	{
		botonSalir = new JButton("Salir");
		botonSalir.setBounds(155, 573, 123, 23);
		botonSalir.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Graficador.resetearRutas(mapaGrafo.getRutas(), mapaPantalla);
				Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				dispose();
			}
		});
		contentPanel.add(botonSalir);
	}
	
	private void setComboBox(Mapa mapaGrafo, JMapViewer mapaPantalla) 
	{
		comboBoxCM = new JComboBox<Trayecto>();
		comboBoxCM = Graficador.llenarComboBoxTrayectos(comboBoxCM, mapaGrafo);
		comboBoxCM.setBounds(124, 92, 300, 20);
		comboBoxCM.setFont(new Font("Tahoma", Font.PLAIN, 12));
		comboBoxCM.setSelectedIndex(-1);
		comboBoxCM.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Graficador.resetearRutas(mapaGrafo.getRutas(), mapaPantalla);
				Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				setDatosCamMin((Trayecto)comboBoxCM.getSelectedItem());
				Graficador.dibujarTrayectoMinimo((Trayecto)comboBoxCM.getSelectedItem(), mapaPantalla);
			}
		});
		contentPanel.add(comboBoxCM);
	}
	
	protected void setDatosCamMin(Trayecto trayecto) 
	{
		n�DistanciaTotal.setText(String.valueOf(trayecto.getDistanciaTotal()));
		n�CantPeajes.setText(String.valueOf(trayecto.getPeajesTotales()));
		n�RutasUtilizadas.setText(String.valueOf(trayecto.getCaminoMinimo().size()));
		n�CiudadesVisitadas.setText(String.valueOf(trayecto.getCiudadesVisitadas().size()));
		textRutas.setText(trayecto.getCaminoMinimo().toString());
		textCiudades.setText(trayecto.getCiudadesVisitadas().toString());
	}
	
	private void setTextAreas()
	{
		textRutas = new JTextArea();
		textRutas.setEditable(false);
		textRutas.setLineWrap(true);
		textRutas.setBounds(64, 257, 360, 134);
		contentPanel.add(textRutas);
		
		textCiudades = new JTextArea();
		textCiudades.setEditable(false);
		textCiudades.setLineWrap(true);
		textCiudades.setBounds(62, 417, 362, 134);
		contentPanel.add(textCiudades);
		
		scroll1 = new JScrollPane (textRutas);
		scroll1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroll1.setBounds(64, 257, 360, 134);
		contentPanel.add(scroll1);
		
		scroll2 = new JScrollPane (textCiudades);
		scroll2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroll2.setBounds(62, 417, 362, 134);
		contentPanel.add(scroll2);
	}
}
