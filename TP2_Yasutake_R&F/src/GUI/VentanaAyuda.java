package GUI;

import java.awt.FlowLayout;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import java.awt.Color;

public class VentanaAyuda extends JDialog 
{

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JLabel string1;
	private JLabel string2;
	private JLabel string3;
	private JLabel string4;
	private JLabel string5;
	private JLabel string6;
	private JLabel string7;
	private JLabel string8;
	private JLabel string9;
	private JLabel string10;
	private JLabel string11;
	private JLabel string12;
	private JLabel string13;
	private JLabel string14;
	private JLabel string15;
	private JLabel string16;
	private JButton botonSalir;
	private JLabel stringExtra1;
	private JLabel stringExtra2;

	public VentanaAyuda() 
	{
		setResizable(false);
		setBounds(100, 100, 450, 555);
		getContentPane().setLayout(null);
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLabels();
	}

	private void setLabels() 
	{
		
		string1 = new JLabel("Mediante esta aplicaci\u00F3n, usted podr\u00E1 agregar ciudades y rutas a voluntad,");
		string1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		string1.setBounds(10, 11, 424, 14);
		getContentPane().add(string1);
		
		string2 = new JLabel("para realizar b\u00FAsquedas sobre rutas que conectan a las ciudades.");
		string2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		string2.setBounds(10, 36, 424, 14);
		getContentPane().add(string2);
		
		string3 = new JLabel("Para esto, cuenta con las opciones de agregar las ciudades manualmente, ");
		string3.setFont(new Font("Tahoma", Font.PLAIN, 12));
		string3.setBounds(10, 61, 424, 14);
		getContentPane().add(string3);
		
		string4 = new JLabel("o bien, haciendo click en el mapa y agregando el nombre deseado. Luego,");
		string4.setFont(new Font("Tahoma", Font.PLAIN, 12));
		string4.setBounds(10, 86, 424, 14);
		getContentPane().add(string4);
		
		string5 = new JLabel("desde menu, podr\u00E1 agregar una ruta con una ciudad de origen y otra de");
		string5.setFont(new Font("Tahoma", Font.PLAIN, 12));
		string5.setBounds(10, 111, 424, 14);
		getContentPane().add(string5);
		
		string6 = new JLabel("destino, indicando adem\u00E1s la distancia total y la existencia de un peaje.");
		string6.setFont(new Font("Tahoma", Font.PLAIN, 12));
		string6.setBounds(10, 136, 424, 14);
		getContentPane().add(string6);
		
		string7 = new JLabel("Sobre \u00E9stas rutas, podr\u00E1 buscar un camino m\u00EDnimo entre las ciudades con");
		string7.setFont(new Font("Tahoma", Font.PLAIN, 12));
		string7.setBounds(10, 211, 424, 14);
		getContentPane().add(string7);
		
		string8 = new JLabel("un m\u00E1ximo numero de peajes a atravesar. A su vez, todo ser\u00E1 visualizado");
		string8.setFont(new Font("Tahoma", Font.PLAIN, 12));
		string8.setBounds(10, 236, 424, 14);
		getContentPane().add(string8);
		
		string9 = new JLabel("en el mapa para su mejor entendimiento.");
		string9.setFont(new Font("Tahoma", Font.PLAIN, 12));
		string9.setBounds(10, 261, 424, 14);
		getContentPane().add(string9);
		
		string10 = new JLabel("Por \u00FAltimo, podr\u00E1 guardar y cargar el mapa luego, remover rutas y ciudades,");
		string10.setFont(new Font("Tahoma", Font.PLAIN, 12));
		string10.setBounds(10, 286, 424, 14);
		getContentPane().add(string10);
		
		string11 = new JLabel("realizar consultas sobre el mapa, tanto de informacion como quita de");
		string11.setFont(new Font("Tahoma", Font.PLAIN, 12));
		string11.setBounds(10, 311, 424, 14);
		getContentPane().add(string11);
		
		string12 = new JLabel("objetos y/o cargar un mapa ejemplo en la ventana como demostraci\u00F3n.");
		string12.setFont(new Font("Tahoma", Font.PLAIN, 12));
		string12.setBounds(10, 336, 424, 14);
		getContentPane().add(string12);
		
		string13 = new JLabel("Puntos azules: Ciudades.");
		string13.setForeground(Color.BLUE);
		string13.setFont(new Font("Tahoma", Font.PLAIN, 12));
		string13.setBounds(152, 375, 139, 14);
		getContentPane().add(string13);
		
		string14 = new JLabel("L\u00EDneas azules: Rutas sin peajes.");
		string14.setForeground(Color.BLUE);
		string14.setFont(new Font("Tahoma", Font.PLAIN, 12));
		string14.setBounds(134, 400, 176, 14);
		getContentPane().add(string14);
		
		string15 = new JLabel("L\u00EDneas rojas: Rutas con peajes.");
		string15.setForeground(Color.RED);
		string15.setFont(new Font("Tahoma", Font.PLAIN, 12));
		string15.setBounds(134, 426, 176, 14);
		getContentPane().add(string15);
		
		string16 = new JLabel("L\u00EDneas cyan: Caminos m\u00EDnimos - rutas seleccionadas.");
		string16.setForeground(Color.CYAN);
		string16.setFont(new Font("Tahoma", Font.BOLD, 12));
		string16.setBounds(58, 454, 327, 14);
		getContentPane().add(string16);
		
		stringExtra1 = new JLabel("La distancia puede ser ingresada manualmente o en km. autom\u00E1ticamente");
		stringExtra1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		stringExtra1.setBounds(10, 161, 424, 14);
		getContentPane().add(stringExtra1);
		
		stringExtra2 = new JLabel("en la ventana. Ser\u00E1 considerada aquella en l\u00EDnea recta.");
		stringExtra2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		stringExtra2.setBounds(10, 186, 424, 14);
		getContentPane().add(stringExtra2);
		
		botonSalir = new JButton("Salir");
		botonSalir.setFont(new Font("Tahoma", Font.PLAIN, 12));
		botonSalir.setBounds(156, 492, 131, 23);
		botonSalir.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
		getContentPane().add(botonSalir);
	}

}
