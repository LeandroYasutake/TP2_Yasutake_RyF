package GUI;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import org.openstreetmap.gui.jmapviewer.JMapViewer;

import Mapa.Mapa;
import Mapa.Ruta;

public class VentanaRemoveRuta extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JLabel titulo;
	private JLabel ciudad;
	private JComboBox<Ruta> rutasCombo;
	private JButton botonRemover;
	private JButton botonCancelar;
	
	public VentanaRemoveRuta(Mapa mapaGrafo, JMapViewer mapaPantalla) 
	{
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setResizable(false);
		setBounds(100, 100, 365, 210);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		setLabels();
		setComboBox(mapaGrafo, mapaPantalla);
		setBotones(mapaGrafo, mapaPantalla);
	}
	
	private void setLabels()
	{
		titulo = new JLabel("Aqui puede remover la ruta deseada:");
		titulo.setFont(new Font("Tahoma", Font.BOLD, 13));
		titulo.setBounds(58, 11, 243, 16);
		contentPanel.add(titulo);
		
		ciudad = new JLabel("Ruta:");
		ciudad.setFont(new Font("Tahoma", Font.PLAIN, 12));
		ciudad.setBounds(15, 71, 46, 16);
		contentPanel.add(ciudad);
	}
	
	private void setComboBox(Mapa mapaGrafo, JMapViewer mapaPantalla)
	{
		rutasCombo = new JComboBox<Ruta>();
		rutasCombo = Graficador.llenarComboBoxRutas(rutasCombo, mapaGrafo);
		rutasCombo.setBounds(82, 70, 267, 20);
		rutasCombo.setSelectedIndex(-1);
		rutasCombo.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Graficador.resetearRutas(mapaGrafo.getRutas(), mapaPantalla);
				Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				Ruta ruta = (Ruta) rutasCombo.getSelectedItem();
				Graficador.dibujarRuta(ruta, mapaPantalla, true);
				Graficador.dibujarParCiudades(ruta, mapaPantalla);
			}
		});
		contentPanel.add(rutasCombo);
	}
	
	private void setBotones(Mapa mapaGrafo, JMapViewer mapaPantalla)
	{
		botonCancelar = new JButton("Cancelar");
		botonCancelar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		botonCancelar.setBounds(209, 136, 89, 23);
		botonCancelar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				Graficador.resetearRutas(mapaGrafo.getRutas(), mapaPantalla);
				dispose();
			}
		});
		contentPanel.add(botonCancelar);
		
		botonRemover = new JButton("Remover ");
		botonRemover.setFont(new Font("Tahoma", Font.PLAIN, 12));
		botonRemover.setBounds(60, 136, 89, 23);
		botonRemover.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Ruta ruta = (Ruta)rutasCombo.getSelectedItem();
				mapaGrafo.removeRuta(ruta);
				Graficador.resetearRutas(mapaGrafo.getRutas(), mapaPantalla);
				String linea1 = ("             Ruta removida!\r\n");
    			JOptionPane.showMessageDialog(null, linea1);
    			Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				dispose();
			}
		});
		contentPanel.add(botonRemover);
	}
}
