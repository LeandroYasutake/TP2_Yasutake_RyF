package GUI;

import java.awt.BorderLayout;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import Mapa.Ciudad;
import Mapa.Mapa;
import Mapa.Trayecto;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JFormattedTextField;
import javax.swing.JButton;

import org.openstreetmap.gui.jmapviewer.JMapViewer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

public class VentanaCaminoMinimo extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JComboBox<Ciudad> origenes;
	private JComboBox<Ciudad> destinos;
	private JLabel titulo;
	private JLabel origen;
	private JLabel destino;
	private JLabel maxPeajes;
	private JLabel distanciaTotal;
	private JLabel distanciaTotalNº;
	private JLabel cantidadDePeajes;
	private JLabel cantidadDePeajesNº;
	private JLabel cantidadDeTramos;
	private JLabel cantidadDeTramosNº;
	private JLabel ciudadesVisitadas;
	private JFormattedTextField maximosPeajes;
	private JTextArea textCiudadesVisitadas;
	private JButton botonGenerarMnimo;
	private JButton botonSalir;
	private JProgressBar progressBar;
	private VentanaCaminoMinimoAlgoritmo procesoCostoso;
	private VentanaCaminoMinimo ventanaCaminoMinimo;
	private JScrollPane scroll;
	
	public VentanaCaminoMinimo(Mapa mapaGrafo, JMapViewer mapaPantalla) 
	{
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		ventanaCaminoMinimo = this;
		setResizable(false);
		setBounds(100, 100, 570, 630);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		setTitle("Búsqueda de Camino Mínimo");
		contentPanel.setLayout(null);
		
		Graficador.resetearRutas(mapaGrafo.getRutas(), mapaPantalla);
		setComboBoxs(mapaGrafo);
		setLabels();
		setText();
		setBotones(mapaGrafo, mapaPantalla);
		
		progressBar = new JProgressBar(0, 100);
		progressBar.setBounds(15, 215, 534, 14);
		contentPanel.add(progressBar);
	}
	
	private void setComboBoxs(Mapa mapaGrafo)
	{
		origenes = new JComboBox<Ciudad>();
		origenes = Graficador.llenarComboBoxCiudades(origenes, mapaGrafo);
		origenes.setFont(new Font("Tahoma", Font.PLAIN, 12));
		origenes.setBounds(132, 71, 152, 22);
		origenes.setSelectedIndex(-1);
		contentPanel.add(origenes);
		
		destinos = new JComboBox<Ciudad>();
		destinos = Graficador.llenarComboBoxCiudades(destinos, mapaGrafo);
		destinos.setFont(new Font("Tahoma", Font.PLAIN, 12));
		destinos.setBounds(392, 71, 152, 23);
		destinos.setSelectedIndex(-1);
		contentPanel.add(destinos);
	}
	
	private void setLabels()
	{
		titulo = new JLabel("Generador de camino m\u00EDnimo");
		titulo.setFont(new Font("Tahoma", Font.BOLD, 13));
		titulo.setBounds(185, 11, 193, 14);
		contentPanel.add(titulo);
		
		origen = new JLabel("Ciudad de Origen:");
		origen.setFont(new Font("Tahoma", Font.PLAIN, 12));
		origen.setBounds(10, 75, 103, 14);
		contentPanel.add(origen);
		
		destino = new JLabel("Ciudad Destino:");
		destino.setFont(new Font("Tahoma", Font.PLAIN, 12));
		destino.setBounds(300, 75, 96, 14);
		contentPanel.add(destino);
		
		maxPeajes = new JLabel("Cantidad m\u00E1xima de peajes:");
		maxPeajes.setFont(new Font("Tahoma", Font.PLAIN, 12));
		maxPeajes.setBounds(120, 153, 152, 14);
		contentPanel.add(maxPeajes);
		
		distanciaTotal = new JLabel("Distancia total: ");
		distanciaTotal.setFont(new Font("Tahoma", Font.PLAIN, 12));
		distanciaTotal.setBounds(10, 257, 89, 14);
		contentPanel.add(distanciaTotal);
		
		distanciaTotalNº = new JLabel("0");
		distanciaTotalNº.setFont(new Font("Tahoma", Font.PLAIN, 12));
		distanciaTotalNº.setHorizontalAlignment(SwingConstants.CENTER);
		distanciaTotalNº.setBounds(162, 257, 69, 14);
		contentPanel.add(distanciaTotalNº);
		
		cantidadDePeajes = new JLabel("Cantidad de Peajes:");
		cantidadDePeajes.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cantidadDePeajes.setBounds(290, 257, 116, 14);
		contentPanel.add(cantidadDePeajes);
		
		cantidadDePeajesNº = new JLabel("0");
		cantidadDePeajesNº.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cantidadDePeajesNº.setHorizontalAlignment(SwingConstants.CENTER);
		cantidadDePeajesNº.setBounds(441, 257, 103, 14);
		contentPanel.add(cantidadDePeajesNº);
		
		cantidadDeTramos = new JLabel("Cantidad de Tramos:");
		cantidadDeTramos.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cantidadDeTramos.setBounds(138, 317, 116, 14);
		contentPanel.add(cantidadDeTramos);
		
		cantidadDeTramosNº = new JLabel("0");
		cantidadDeTramosNº.setHorizontalAlignment(SwingConstants.CENTER);
		cantidadDeTramosNº.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cantidadDeTramosNº.setBounds(314, 317, 68, 14);
		contentPanel.add(cantidadDeTramosNº);
		
		ciudadesVisitadas = new JLabel("Ciudades Visitadas:");
		ciudadesVisitadas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		ciudadesVisitadas.setBounds(10, 371, 102, 14);
		contentPanel.add(ciudadesVisitadas);
	}
	
	private void setText()
	{
		maximosPeajes = new JFormattedTextField();
		maximosPeajes.setFont(new Font("Tahoma", Font.PLAIN, 12));
		maximosPeajes.setHorizontalAlignment(SwingConstants.CENTER);
		maximosPeajes.setBounds(297, 151, 103, 20);
		contentPanel.add(maximosPeajes);
		
		textCiudadesVisitadas = new JTextArea();
		textCiudadesVisitadas.setLineWrap(true);
		textCiudadesVisitadas.setEditable(false);
		textCiudadesVisitadas.setBounds(-147, -64, 421, 159);
		contentPanel.add(textCiudadesVisitadas);
		
		scroll = new JScrollPane (textCiudadesVisitadas);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroll.setBounds(115, 370, 429, 156);
		contentPanel.add(scroll);
	}
	
	private void setBotones(Mapa mapaGrafo, JMapViewer mapaPantalla)
	{	
		botonSalir = new JButton("Salir");
		botonSalir.setFont(new Font("Tahoma", Font.PLAIN, 12));
		botonSalir.setBounds(333, 568, 129, 23);
		contentPanel.add(botonSalir);
		botonSalir.setActionCommand("Cancel");
		botonSalir.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Graficador.resetearRutas(mapaGrafo.getRutas(), mapaPantalla);
				Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				dispose();
			}
		});
		contentPanel.add(botonSalir);
		
		botonGenerarMnimo = new JButton("Generar M\u00EDnimo");
		botonGenerarMnimo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		botonGenerarMnimo.setBounds(102, 568, 129, 23);
		botonGenerarMnimo.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Graficador.resetearRutas(mapaGrafo.getRutas(), mapaPantalla);
				Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				Ciudad origen = (Ciudad)origenes.getSelectedItem();
				Ciudad destino = (Ciudad)destinos.getSelectedItem();
				
				if (origen.equals(destino))
				{
					String nl = System.getProperty("line.separator");
					String linea1 = ("Ruta mínima incorrecta. Mismo origen y destino.\r\n");
					String linea2 = ("                 Por favor, eliga una nueva.");
					JOptionPane.showMessageDialog(null, linea1+nl+linea2);
				}
				
				else
				{
					Pattern patternPeajeMaximos = Pattern.compile("[0-9]+");
					Matcher matcherPeajesMaximos = patternPeajeMaximos.matcher(maximosPeajes.getText());
					
					if (!matcherPeajesMaximos.matches())
					{
						String linea1 = ("Por favor, ingrese un número de peajes máximos correctos.\r\n");
						JOptionPane.showMessageDialog(null, linea1);
					}
					
					else
					{
						procesoCostoso = new VentanaCaminoMinimoAlgoritmo (mapaGrafo, origen, destino, 
								Integer.parseInt(maximosPeajes.getText()), ventanaCaminoMinimo);
						procesoCostoso.execute();
						setpropertyChange(procesoCostoso, mapaPantalla);
					}
				}
			}
		});
		contentPanel.add(botonGenerarMnimo);
	}
	
	protected void setpropertyChange(VentanaCaminoMinimoAlgoritmo procesoCostoso2, JMapViewer mapaPantalla) 
	{
		procesoCostoso.addPropertyChangeListener(new PropertyChangeListener() 
		{
			@Override
			public void propertyChange(PropertyChangeEvent evt) 
			{
				if ("state".equals(evt.getPropertyName())) 
				{
					if (evt.getNewValue() == SwingWorker.StateValue.DONE) 
					{
						try 
						{
							Trayecto camMinimo  = procesoCostoso.get();
							Graficador.dibujarTrayectoMinimo(camMinimo, mapaPantalla);
							setValoresFinales(camMinimo);
							String linea1 = ("    Camino mínimo encontrado!\r\n");
							JOptionPane.showMessageDialog(null, linea1);
						} catch (Exception e) 
						{
							resetearLabels();
							String linea1 = ("No hubo camino minimo posible.\r\n");
							JOptionPane.showMessageDialog(null, linea1);
						}
					}
				}
			}
		});
	}

	private void setValoresFinales(Trayecto minimo)
	{
		distanciaTotalNº.setText(String.valueOf(minimo.getDistanciaTotal()));
		cantidadDePeajesNº.setText(String.valueOf(minimo.getPeajesTotales()));
		cantidadDeTramosNº.setText(String.valueOf(minimo.getCaminoMinimo().size()));
		textCiudadesVisitadas.setText(minimo.getCiudadesVisitadas().toString());
	}
	
	public JProgressBar getProgressBar()
	{
		return this.progressBar;
	}
	
	private void resetearLabels()
	{
		distanciaTotalNº.setText("0");
		cantidadDePeajesNº.setText("0");
		cantidadDeTramosNº.setText("0");
	}
}
