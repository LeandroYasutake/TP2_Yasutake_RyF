package GUI;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;

import Mapa.Ciudad;
import Mapa.Mapa;

public class VentanaAddCiudadManual extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField textNombre;
	private JFormattedTextField textLat;
	private JFormattedTextField textLong;
	private JLabel titulo;
	private JLabel nombre;
	private JLabel latitud;
	private JLabel longitud;
	private JButton botonAgregarCiudad;
	private JButton botonCancelar;
	
	public VentanaAddCiudadManual(JMapViewer mapaPantalla, Mapa mapa) 
	{
		setResizable(false);
		setBounds(100, 100, 486, 335);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		setLabels();
		setTextFields();
		setBotones(mapaPantalla, mapa);
	}
	
	private void setLabels()
	{
		titulo = new JLabel("Aqu\u00ED puede agregar la ciudad deseada manualmente:");
		titulo.setFont(new Font("Tahoma", Font.BOLD, 13));
		titulo.setBounds(67, 11, 345, 14);
		contentPanel.add(titulo);
		
		nombre = new JLabel("Nombre:");
		nombre.setFont(new Font("Tahoma", Font.PLAIN, 12));
		nombre.setBounds(10, 76, 60, 14);
		contentPanel.add(nombre);
		
		latitud = new JLabel("Latitud:");
		latitud.setFont(new Font("Tahoma", Font.PLAIN, 12));
		latitud.setBounds(10, 135, 60, 14);
		contentPanel.add(latitud);
		
		longitud = new JLabel("Longitud:");
		longitud.setFont(new Font("Tahoma", Font.PLAIN, 12));
		longitud.setBounds(10, 200, 60, 14);
		contentPanel.add(longitud);
	}
	
	private void setTextFields()
	{
		textNombre = new JTextField();
		textNombre.setHorizontalAlignment(SwingConstants.CENTER);
		textNombre.setFont(new Font("Tahoma", Font.PLAIN, 12));
		textNombre.setBounds(160, 70, 310, 20);
		contentPanel.add(textNombre);
		textNombre.setColumns(10);
		
		textLat = new JFormattedTextField();
		textLat.setFont(new Font("Tahoma", Font.PLAIN, 12));
		textLat.setHorizontalAlignment(SwingConstants.CENTER);
		textLat.setBounds(160, 129, 310, 20);
		contentPanel.add(textLat);
		textLat.setColumns(10);
		
		textLong = new JFormattedTextField();
		textLong.setFont(new Font("Tahoma", Font.PLAIN, 12));
		textLong.setHorizontalAlignment(SwingConstants.CENTER);
		textLong.setBounds(160, 194, 310, 20);
		contentPanel.add(textLong);
		textLong.setColumns(10);
	}
	
	private void setBotones(JMapViewer mapaPantalla, Mapa mapa)
	{
		botonCancelar = new JButton("Cancelar");
		botonCancelar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		botonCancelar.setBounds(273, 273, 141, 23);
		botonCancelar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
		contentPanel.add(botonCancelar);
		
		botonAgregarCiudad = new JButton("Agregar Ciudad");
		botonAgregarCiudad.setFont(new Font("Tahoma", Font.PLAIN, 12));
		botonAgregarCiudad.setBounds(66, 272, 141, 23);
		botonAgregarCiudad.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Pattern patternNombre = Pattern.compile("");
				Pattern patternLat = Pattern.compile("(\\+|\\-)?(?:90(?:(?:\\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\\.[0-9]{1,6})?))");
				Pattern patternLong = Pattern.compile("(\\+|\\-)?(?:180(?:(?:\\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\\.[0-9]{1,6})?))");
				
				Matcher matcherNombre = patternNombre.matcher(textNombre.getText());
				Matcher matcherLat = patternLat.matcher(textLat.getText());
				Matcher matcherLong = patternLong.matcher(textLong.getText());
				
				if (matcherNombre.matches())
				{
					String linea1 = ("Por favor, ingrese un nombre correcto para la ciudad deseada.\r\n");
					JOptionPane.showMessageDialog(null, linea1);
				}
						
				else if(!matcherLat.matches())
				{
					String linea1 = ("Por favor, ingrese una latitud correcta.\r\n");
					JOptionPane.showMessageDialog(null, linea1);
				}
			
				else if (!matcherLong.matches())
				{
					String linea1 = ("Por favor, ingrese una longitud correcta.\r\n");
					JOptionPane.showMessageDialog(null, linea1);
				}
						
				else
				{
					Coordinate coordenada = new Coordinate(Double.parseDouble(textLat.getText()), Double.parseDouble(textLong.getText())); 
					Ciudad ciudad = new Ciudad(textNombre.getText(), coordenada.getLat(), coordenada.getLon());
								
					if (!mapa.getCiudades().contains(ciudad))
					{
						mapa.addCiudad(ciudad);
					       Graficador.agregarMarcador(ciudad, mapaPantalla, Color.BLUE);
					       String linea1 = ("           Ciudad agregada!\r\n");
					       JOptionPane.showMessageDialog(null, linea1);
					       dispose();	
					}
					else
					{
						String linea1 = ("  La ciudad ingresada ya existe, por favor ingrese otra.\r\n");
						JOptionPane.showMessageDialog(null, linea1);
					}
				}
			}	
		});
		contentPanel.add(botonAgregarCiudad);
	}
}
