package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Mapa.Ciudad;
import Mapa.Mapa;
import Mapa.Ruta;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;

import org.openstreetmap.gui.jmapviewer.JMapViewer;

public class VentanaAddRuta extends JDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JButton botonCancelar;
	private JButton botonAgregarRuta;
	private JComboBox<Ciudad> origenes;
	private JComboBox<Ciudad> destinos;
	private JLabel texto1;
	private JLabel texto2;
	private JLabel distancia;
	private JLabel origen;
	private JLabel destino;
	private JCheckBox hayPeaje;
	private JCheckBox calcularDistancia;
	private JFormattedTextField distanciaInput;
	
	public VentanaAddRuta(Mapa mapaGrafo, JMapViewer mapaPantalla) 
	{
		setResizable(false);
		setBounds(100, 100, 590, 347);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		setLabels();
		setComboBoxs(mapaGrafo, mapaPantalla);
		setCheckYtext(mapaGrafo);
		setBotones(mapaGrafo, mapaPantalla);	
	}
	
	private void setLabels()
	{
		texto1 = new JLabel("Por favor, ingrese la ruta deseada indicando la ciudad de origen, ");
		texto1.setBounds(80, 21, 424, 23);
		texto1.setFont(new Font("Tahoma", Font.BOLD, 13));
		texto1.setHorizontalAlignment(SwingConstants.CENTER);
		contentPanel.add(texto1);
		
		texto2 = new JLabel(" destino y la existencia de peaje:");
		texto2.setBounds(80, 55, 424, 14);
		texto2.setFont(new Font("Tahoma", Font.BOLD, 13));
		texto2.setHorizontalAlignment(SwingConstants.CENTER);
		contentPanel.add(texto2);
		
		distancia = new JLabel("Distancia:");
		distancia.setBounds(309, 212, 64, 14);
		distancia.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPanel.add(distancia);
		
		origen = new JLabel("Ciudad de Origen:");
		origen.setBounds(16, 129, 106, 14);
		origen.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPanel.add(origen);
		
		destino = new JLabel("Ciudad Destino:");
		destino.setBounds(309, 129, 85, 14);
		destino.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPanel.add(destino);
	}
	
	private void setComboBoxs(Mapa mapaGrafo, JMapViewer mapaPantalla)
	{
		origenes = new JComboBox<Ciudad>();
		origenes = Graficador.llenarComboBoxCiudades(origenes, mapaGrafo);
		origenes.setFont(new Font("Tahoma", Font.PLAIN, 12));
		origenes.setBounds(138, 121, 155, 22);
		origenes.setSelectedIndex(-1);
		origenes.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				Ciudad ciudad = (Ciudad) origenes.getSelectedItem();
				Graficador.agregarMarcador(ciudad, mapaPantalla, Color.YELLOW);
			}
		});
		contentPanel.add(origenes);
		
		destinos = new JComboBox<Ciudad>();
		destinos = Graficador.llenarComboBoxCiudades(destinos, mapaGrafo);
		destinos.setFont(new Font("Tahoma", Font.PLAIN, 12));
		destinos.setBounds(410, 121, 155, 22);
		destinos.setSelectedIndex(-1);
		destinos.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				Ciudad ciudad = (Ciudad) destinos.getSelectedItem();
				Graficador.agregarMarcador(ciudad, mapaPantalla, Color.YELLOW);
			}
		});
		contentPanel.add(destinos);
	}
	
	private void setCheckYtext(Mapa mapaGrafo)
	{
		hayPeaje = new JCheckBox("Hay peaje?");
		hayPeaje.setHorizontalAlignment(SwingConstants.CENTER);
		hayPeaje.setBounds(16, 209, 85, 23);
		hayPeaje.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPanel.add(hayPeaje);
		
		calcularDistancia = new JCheckBox("Calcular distancia? (Km.)");
		calcularDistancia.setHorizontalAlignment(SwingConstants.CENTER);
		calcularDistancia.setFont(new Font("Tahoma", Font.PLAIN, 12));
		calcularDistancia.setBounds(127, 209, 166, 23);
		calcularDistancia.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (calcularDistancia.isSelected())
				{
					double distaciaReal = mapaGrafo.getDistanciaReal((Ciudad)origenes.getSelectedItem(), (Ciudad)destinos.getSelectedItem());
					distanciaInput.setText(""+distaciaReal);
				}
			}
		});
		contentPanel.add(calcularDistancia);
		
		distanciaInput = new JFormattedTextField();
		distanciaInput.setFont(new Font("Tahoma", Font.PLAIN, 12));
		distanciaInput.setHorizontalAlignment(SwingConstants.CENTER);
		distanciaInput.setBounds(410, 209, 155, 20);
		contentPanel.add(distanciaInput);
	}
	
	private void setBotones(Mapa mapaGrafo, JMapViewer mapaPantalla)
	{
		botonCancelar = new JButton("Salir");
		botonCancelar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		botonCancelar.setBounds(341, 284, 143, 23);
		contentPanel.add(botonCancelar);
		botonCancelar.setActionCommand("Cancel");
		botonCancelar.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				dispose();
			}
		});
		
		botonAgregarRuta = new JButton("Agregar ruta");
		botonAgregarRuta.setFont(new Font("Tahoma", Font.PLAIN, 12));
		botonAgregarRuta.setBounds(99, 284, 143, 23);
		botonAgregarRuta.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Pattern patternDistancia = Pattern.compile("(0*[1-9][0-9]*(\\.[0-9]+)?|0+\\.[0-9]*[1-9][0-9]*)");
				Matcher matcherDistancia = patternDistancia.matcher(distanciaInput.getText());
				
				if (!matcherDistancia.matches())
				{
					String linea1 = ("Por favor, ingrese una distancia correcta.\r\n");
					JOptionPane.showMessageDialog(null, linea1);
				}
				
				else
					crearRuta(mapaGrafo, origenes.getSelectedItem(), destinos.getSelectedItem(), hayPeaje.isSelected(), 
							  mapaPantalla, Double.parseDouble(distanciaInput.getText()));
			}
		});
		contentPanel.add(botonAgregarRuta);
	}
	
	private void crearRuta(Mapa mapaGrafo, Object object, Object object2, boolean peaje, JMapViewer mapaPantalla, double distancia) 
	{
		int hayPeaje = 0;
		Ciudad origen = (Ciudad)object;
		Ciudad destino = (Ciudad)object2;
			
		if (peaje)
			hayPeaje = 1;
		
		if (origen.equals(destino))
			showErrorEquals();
		
		else
		{
			Ruta ruta = new Ruta (origen, destino, distancia, hayPeaje);
			
			if (!mapaGrafo.getRutas().contains(ruta))
			{
				aņadirRuta(ruta, mapaGrafo, mapaPantalla);
				String linea1 = ("              Ruta creada!\r\n");
				JOptionPane.showMessageDialog(null, linea1);
				Graficador.resetearCiudades(mapaGrafo.getCiudades(), mapaPantalla);
				this.hayPeaje.setSelected(false);
				this.calcularDistancia.setSelected(false);
				distanciaInput.setText(""+0);
			}	
			
			else{
				this.hayPeaje.setSelected(false);
				calcularDistancia.setSelected(false);
				distanciaInput.setText(""+0);
				showErrorRutaExistente();
			}
		}
	}
	
	private void showErrorEquals()
	{
		String nl = System.getProperty("line.separator");
		String linea1 = ("     La ruta ingresada no puede contener el mismo origen y destino.\r\n");
		String linea2 = ("                                    Por favor, eliga una nueva.");
		JOptionPane.showMessageDialog(null, linea1+nl+linea2);
	}
	
	private void aņadirRuta(Ruta ruta, Mapa mapaGrafo, JMapViewer mapaPantalla)
	{
		mapaGrafo.addRuta(ruta);
		Graficador.dibujarRuta(ruta, mapaPantalla, false);
	}
	
	private void showErrorRutaExistente()
	{
		String nl = System.getProperty("line.separator");
		String linea1 = ("     La ruta ingresada ya existe.\r\n");
		String linea2 = ("     Por favor, eliga una nueva.");
		JOptionPane.showMessageDialog(null, linea1+nl+linea2);
	}
}
