package GrafosTest;

import static org.junit.Assert.*;

import org.junit.Test;

import Grafos.Dijkstra;
import Grafos.GrafoPesado;

import java.util.ArrayList;
import java.util.List;

public class DijkstraTest 
{

	@SuppressWarnings("unused")
	@Test
	public void crearDijkstra() 
	{
		GrafoPesado grafo = new GrafoPesado(3);
		Dijkstra algoritmo = new Dijkstra(grafo);
	}
	
	public void algoritmo()
	{
		GrafoPesado grafo = new GrafoPesado(3);
		Dijkstra algoritmo = new Dijkstra(grafo);
		List<Integer> camMin = new ArrayList<Integer>();
		
		grafo.agregarArista(0, 1, 1);
		grafo.agregarArista(0, 2, 1);
		grafo.agregarArista(2, 3, 1);
		camMin.add(0);
		camMin.add(1);
		
		assertTrue(camMin.equals(algoritmo.caminoMinimo(0, 1)));
	}
}
