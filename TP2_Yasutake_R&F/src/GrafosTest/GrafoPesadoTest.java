package GrafosTest;

import static org.junit.Assert.*;

import org.junit.Test;

import Grafos.Grafo;
import Grafos.GrafoPesado;


public class GrafoPesadoTest 
{

	@SuppressWarnings("unused")
	@Test
	public void grafoPesadoTest() 
	{
		GrafoPesado grafo = new GrafoPesado(2);
	}
	
	@Test
	public void agregarAristaTest()
	{
		GrafoPesado grafo = new GrafoPesado(2);
		
		grafo.agregarArista(0, 1, 1.5);
		assertTrue(grafo.contieneArista(0, 1));
		assertEquals(1.5, grafo.pesoArista(0, 1), 1e-4);
	}
	
	@Test
	public void simetriaPesoArista()
	{
		GrafoPesado grafo = new GrafoPesado(2);
		
		grafo.agregarArista(0, 1, 1.5);
		assertEquals(1.5, grafo.pesoArista(0, 1), 1e-4);
		assertEquals(1.5, grafo.pesoArista(1, 0), 1e-4);
	}

	@Test
	public void sonVecinos()
	{
		Grafo grafo = new Grafo(3);

		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(1, 0);

		assertFalse(grafo.sonVecinos(2, 1));
		assertFalse(grafo.sonVecinos(1, 2));
		assertTrue(grafo.sonVecinos(0, 1));
		assertTrue(grafo.sonVecinos(1, 0));
		assertTrue(grafo.sonVecinos(0, 2));
		assertTrue(grafo.sonVecinos(2, 0));
	}

}
