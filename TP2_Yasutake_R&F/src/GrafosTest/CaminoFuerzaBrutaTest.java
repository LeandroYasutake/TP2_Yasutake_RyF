package GrafosTest;

import static org.junit.Assert.*;

import org.junit.Test;

import Grafos.CaminoFuerzaBruta;
import Grafos.GrafoPesado;
import Mapa.Ciudad;
import Mapa.Ruta;
import Mapa.Trayecto;

public class CaminoFuerzaBrutaTest 
{

	@Test
	public void FuerzaBruta() 
	{
		GrafoPesado grafo = new GrafoPesado(3);
		Ruta[][] matrizRutasCiudades = new Ruta[3][3];
		Ciudad ciudad = new Ciudad("x", 0, 0);
		Ciudad ciudad2 = new Ciudad("y", 0, 0);
		Ciudad ciudad3 = new Ciudad("z", 0, 0);
		Ruta ruta = new Ruta(ciudad, ciudad2, 1, 1);
		Ruta ruta2 = new Ruta(ciudad, ciudad3, 1, 0);
		Ruta ruta3 = new Ruta(ciudad2, ciudad3, 1, 0);
		Trayecto camMinConPeaje = new Trayecto();
		Trayecto camMinSinPeaje = new Trayecto();
		CaminoFuerzaBruta camino;
		
		grafo.agregarArista(0, 1, 1);
		grafo.agregarArista(0, 2, 1);
		grafo.agregarArista(1, 2, 1);
		matrizRutasCiudades[0][1] = ruta;
		matrizRutasCiudades[1][0] = ruta;
		matrizRutasCiudades[0][2] = ruta2;
		matrizRutasCiudades[2][0] = ruta2;
		matrizRutasCiudades[1][2] = ruta3;
		matrizRutasCiudades[2][1] = ruta3;
		camMinConPeaje.addSubTrayecto(ruta);
		camMinSinPeaje.addSubTrayecto(ruta2);
		camMinSinPeaje.addSubTrayecto(ruta3);
		
		camino = new CaminoFuerzaBruta(1, grafo, matrizRutasCiudades);
		camino.encontrarTrayectoMinimoFuerzaBruta(0, 1);
		
		assertEquals(camino.getCaminoMinimo().getDistanciaTotal(), camMinConPeaje.getDistanciaTotal(), 0);
		assertEquals(camino.getCaminoMinimo().getPeajesTotales(), camMinConPeaje.getPeajesTotales(), 0);
		assertEquals(camino.getCaminoMinimo().getCiudadesVisitadas(), camMinConPeaje.getCiudadesVisitadas());
		
		camino = new CaminoFuerzaBruta(0, grafo, matrizRutasCiudades);
		camino.encontrarTrayectoMinimoFuerzaBruta(0, 1);
		
		assertEquals(camino.getCaminoMinimo().getDistanciaTotal(), camMinSinPeaje.getDistanciaTotal(), 0);
		assertEquals(camino.getCaminoMinimo().getPeajesTotales(), camMinSinPeaje.getPeajesTotales(), 0);
		assertEquals(camino.getCaminoMinimo().getCiudadesVisitadas(), camMinSinPeaje.getCiudadesVisitadas());
	}

}
