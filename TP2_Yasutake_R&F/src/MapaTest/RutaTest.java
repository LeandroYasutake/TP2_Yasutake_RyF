package MapaTest;

import static org.junit.Assert.*;

import org.junit.Test;

import Mapa.Ciudad;
import Mapa.Ruta;

public class RutaTest 
{

	@Test (expected = AssertionError.class)
	public void crearRuta() 
	{
		Ciudad ciudad1 = new Ciudad("x", 0 , 0);
		Ciudad ciudad2 = new Ciudad("y", 0 , 0);
		Ruta ruta = new Ruta(ciudad1, ciudad2, 500, 1);
		
		assertEquals(ciudad1, ruta.getOrigen());
		assertEquals(ciudad2, ruta.getDestino());
		assertEquals(500, ruta.getDistancia(), 0);
		assertEquals(1, ruta.getPeaje(), 0);
		
		assertEquals(ciudad2, ruta.getOrigen());  	//error
		assertEquals(ciudad1, ruta.getDestino());	//error
		assertEquals(0, ruta.getPeaje(), 0);		//error
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void setDistancia() 
	{
		Ciudad ciudad1 = new Ciudad("x", 0 , 0);
		Ciudad ciudad2 = new Ciudad("y", 0 , 0);
		Ruta ruta = new Ruta(ciudad1, ciudad2, 500, 1);
		
		ruta.setDistancia(0);
		ruta.setDistancia(-1);
	}
	
	@Test 
	public void equals() 
	{
		Ciudad ciudad1 = new Ciudad("x", 0 , 0);
		Ciudad ciudad2 = new Ciudad("y", 0 , 0);
		Ruta ruta = new Ruta(ciudad1, ciudad2, 500, 1);
		Ruta ruta2 = new Ruta(ciudad1, ciudad2, 500, 1);
		Ruta ruta3 = new Ruta(ciudad2, ciudad1, 500, 1);
		Ruta ruta4 = new Ruta(ciudad2, ciudad1, 25, 0);
		
		assertTrue(ruta.equals(ruta2));
		assertTrue(ruta.equals(ruta3));
		assertTrue(ruta.equals(ruta4)); //no considero distancias/peaje
	}
}
