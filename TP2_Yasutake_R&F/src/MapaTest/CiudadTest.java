package MapaTest;

import static org.junit.Assert.*;

import org.junit.Test;

import Mapa.Ciudad;

public class CiudadTest 
{

	@SuppressWarnings("unused")
	@Test (expected = IllegalArgumentException.class)
	public void crearCiudad() 
	{
		Ciudad ciudad = new Ciudad("x", 15.35, 35.15);
		Ciudad ciudad2 = new Ciudad("", 15.35, 35.15); //sin nombre
		Ciudad ciudad3 = new Ciudad("z", -1, 181);
		Ciudad ciudad4 = new Ciudad("y", 91, -1);
		
		assertEquals("x", ciudad.getNombre());
		assertEquals(15.35, ciudad.getLatitud(), 0);
		assertEquals(35.15, ciudad.getLongitud(), 0);
	}

	public void equals() 
	{
		Ciudad ciudad = new Ciudad("x", 15.35, 35.15);
		Ciudad ciudad2 = new Ciudad("y", 15.35, 35.15); //sin nombre
		
		assertFalse(ciudad.equals(ciudad2));
	}
}
