package MapaTest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import Mapa.Ciudad;
import Mapa.Mapa;
import Mapa.Ruta;
import Mapa.Trayecto;

public class MapaTest 
{
	@Test (expected = NullPointerException.class)
	public void crearMapa() 
	{
		Mapa mapa = new Mapa();
		
		assertEquals(0, mapa.getRutasSP());
		assertEquals(0, mapa.getRutasCP());
		assertTrue(mapa.getCiudades().isEmpty());
		assertTrue(mapa.getTrayectos().isEmpty());
		assertTrue(mapa.getRutas().isEmpty());
		assertEquals(0, mapa.getMatrizRutasCiudades().length); //null
		assertEquals(0, mapa.getId());
	}
	
	@Test (expected = AssertionError.class)
	public void agregarCiudad() 
	{
		Mapa mapa = new Mapa();
		Ciudad ciudad = new Ciudad("x", 0, 0);
		
		mapa.addCiudad(ciudad);
		
		assertEquals(0, mapa.getCiudades().size()); //error
		assertEquals(1, mapa.getCiudades().size());
		assertEquals(2, mapa.getCiudades().size()); //error
		assertEquals(0, mapa.getId()); //error
		assertEquals(1, mapa.getId());
		assertEquals(2, mapa.getId()); //error
	}

	@Test (expected = AssertionError.class)
	public void removerCiudad() 
	{
		Mapa mapa = new Mapa();
		Ciudad ciudad = new Ciudad("x", 0, 0);
		
		mapa.addCiudad(ciudad);
		mapa.removeCiudad(ciudad);
		
		assertEquals(0, mapa.getCiudades().size());
		assertEquals(1, mapa.getCiudades().size());  //error
		assertEquals(1, mapa.getId()); 
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void removerCiudadNoAgregada() 
	{
		Mapa mapa = new Mapa();
		Ciudad ciudad = new Ciudad("x", 0, 0);
		
		mapa.removeCiudad(ciudad);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void removerCiudadConRutas() 
	{
		Mapa mapa = new Mapa();
		Ciudad ciudad = new Ciudad("x", 0, 0);
		Ciudad ciudad2 = new Ciudad("y", 0, 0);
		Ruta ruta = new Ruta(ciudad, ciudad2, 0, 0); 
		
		mapa.addRuta(ruta);
		mapa.addCiudad(ciudad);
		mapa.addCiudad(ciudad2);
		mapa.removeCiudad(ciudad);
		
		assertEquals(1, mapa.getCiudades().size());
		assertEquals(0, mapa.getRutasSP());         //al remover la ciudad remueve la ruta
	}
	
	@Test (expected = AssertionError.class)
	public void getPosicionCiudadEnArreglo() 
	{
		Mapa mapa = new Mapa();
		Ciudad ciudad = new Ciudad("x", 0, 0);
		Ciudad ciudad2 = new Ciudad("y", 0, 0);
		Ciudad ciudad3 = new Ciudad("z", 0, 0);
		
		mapa.addCiudad(ciudad);
		mapa.addCiudad(ciudad2);
		mapa.addCiudad(ciudad3);
		
		assertEquals(0, mapa.getPosCiudad(ciudad));
		assertEquals(1, mapa.getPosCiudad(ciudad2)); 
		assertEquals(2, mapa.getPosCiudad(ciudad3));
		
		assertEquals(2, mapa.getPosCiudad(ciudad));
		assertEquals(1, mapa.getPosCiudad(ciudad2));	//error
		assertEquals(0, mapa.getPosCiudad(ciudad3));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarRuta() 
	{
		Mapa mapa = new Mapa();
		Ciudad ciudad = new Ciudad("x", 0, 0);
		Ciudad ciudad2 = new Ciudad("y", 0, 0);
		Ciudad ciudad3 = new Ciudad("z", 0, 0);
		Ruta ruta = new Ruta(ciudad, ciudad2, 1, 1);
		Ruta ruta2 = new Ruta(ciudad, ciudad3, 1, 0);
		
		mapa.addRuta(ruta);
		mapa.addRuta(ruta2);
		
		assertEquals(1, mapa.getRutasSP());
		assertEquals(1, mapa.getRutasCP());
		
		mapa.addRuta(ruta); //controlo ruta ya existente
	}
	
	@Test
	public void removerRuta() 
	{
		Mapa mapa = new Mapa();
		Ciudad ciudad = new Ciudad("x", 0, 0);
		Ciudad ciudad2 = new Ciudad("y", 0, 0);
		Ciudad ciudad3 = new Ciudad("z", 0, 0);
		Ruta ruta = new Ruta(ciudad, ciudad2, 1, 1);
		Ruta ruta2 = new Ruta(ciudad, ciudad3, 1, 0);
		
		mapa.addRuta(ruta);
		mapa.addRuta(ruta2);
		
		assertEquals(1, mapa.getRutasSP());
		assertEquals(1, mapa.getRutasCP());
		
		mapa.removeRuta(ruta);
		mapa.removeRuta(ruta2);
		
		assertEquals(0, mapa.getRutasSP());
		assertEquals(0, mapa.getRutasCP());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void caminoMinimo() 
	{
		Mapa mapa = new Mapa();
		Ciudad ciudad = new Ciudad("x", 0, 0);
		Ciudad ciudad2 = new Ciudad("y", 0, 0);
		Ciudad ciudad3 = new Ciudad("z", 0, 0);
		Ruta ruta = new Ruta(ciudad, ciudad2, 1, 1);
		Ruta ruta2 = new Ruta(ciudad, ciudad3, 1, 0);
		Ruta ruta3 = new Ruta(ciudad2, ciudad3, 1, 0);
		Trayecto caminoMinimo = null;
		int peajesMaximos = 0;
		
		mapa.addCiudad(ciudad);
		mapa.addCiudad(ciudad2);
		mapa.addCiudad(ciudad3);
		mapa.addRuta(ruta);
		mapa.addRuta(ruta2);
		mapa.addRuta(ruta3);
		
		caminoMinimo = mapa.caminoMinimo(mapa.getPosCiudad(ciudad), mapa.getPosCiudad(ciudad2), peajesMaximos);
		
		assertEquals(2, caminoMinimo.getDistanciaTotal(), 0);
		assertEquals(0, caminoMinimo.getPeajesTotales(), 0);
		
		peajesMaximos = 1;
		caminoMinimo = mapa.caminoMinimo(mapa.getPosCiudad(ciudad), mapa.getPosCiudad(ciudad2), peajesMaximos);
		
		assertEquals(1, caminoMinimo.getPeajesTotales(), 0);
		assertEquals(1, caminoMinimo.getDistanciaTotal(), 0);
		
		mapa.removeRuta(ruta2);
		mapa.removeRuta(ruta3);
		
		caminoMinimo = mapa.caminoMinimo(mapa.getPosCiudad(ciudad), mapa.getPosCiudad(ciudad3), peajesMaximos);
		//sin camino a ciudad 3
		
		//en este test, tambien testeamos la clase Constructor (metodos estaticos), tanto
		//con List<> como con Stack<>. A su vez, tambien
		//testeamos las clases Dijkstra y CaminoFuerzaBruta dado
		//que instanciamos dichas clases en los metodos de busqueda.
	}
	
	@Test
	public void resetearMapa() 
	{
		Mapa mapa = new Mapa();
		Ciudad ciudad = new Ciudad("x", 0, 0);
		Ciudad ciudad2 = new Ciudad("y", 0, 0);
		Ruta ruta = new Ruta(ciudad, ciudad2, 1, 1);
		
		mapa.addCiudad(ciudad);
		mapa.addCiudad(ciudad2);
		mapa.addRuta(ruta);
		
		mapa.resetearMapa(true);
		
		assertEquals(0, mapa.getCiudades().size());
		assertEquals(0, mapa.getRutas().size()); 
		assertEquals(0, mapa.getId());
		
		mapa.addCiudad(ciudad);
		mapa.addCiudad(ciudad2);
		mapa.addRuta(ruta);
		
		mapa.resetearMapa(false);
		assertEquals(0, mapa.getRutas().size());
		assertEquals(2, mapa.getCiudades().size());
		assertEquals(2, mapa.getId());
	}
	
	@Test (expected = AssertionError.class)
	public void numeroVecinos() 
	{
		Mapa mapa = new Mapa();
		Ciudad ciudad = new Ciudad("x", 0, 0);
		Ciudad ciudad2 = new Ciudad("y", 0, 0);
		Ciudad ciudad3 = new Ciudad("z", 0, 0);
		Ruta ruta = new Ruta(ciudad, ciudad2, 1, 1);
		Ruta ruta2 = new Ruta(ciudad, ciudad3, 1, 0);
		
		mapa.addRuta(ruta);
		mapa.addRuta(ruta2);
		
		assertEquals(2, mapa.numeroVecinos(ciudad));
		assertEquals(1, mapa.numeroVecinos(ciudad2));
		assertEquals(1, mapa.numeroVecinos(ciudad2));
		assertEquals(0, mapa.numeroVecinos(ciudad));
		assertEquals(0, mapa.numeroVecinos(ciudad2));
		assertEquals(0, mapa.numeroVecinos(ciudad2));
	}
	
	@Test 
	public void getVecinosString() 
	{
		Mapa mapa = new Mapa();
		Ciudad ciudad = new Ciudad("x", 0, 0);
		Ciudad ciudad2 = new Ciudad("y", 0, 0);
		Ciudad ciudad3 = new Ciudad("z", 0, 0);
		Ruta ruta = new Ruta(ciudad, ciudad2, 1, 1);
		Ruta ruta2 = new Ruta(ciudad, ciudad3, 1, 0);
		String vecinosCiudad = " y z";
		String vecinosCiudad2 = " x";
		String vecinosCiudad3 = " x";
		
		mapa.addRuta(ruta);
		mapa.addRuta(ruta2);
		
		assertEquals(vecinosCiudad, mapa.getVecinos(ciudad));
		assertEquals(vecinosCiudad2, mapa.getVecinos(ciudad2));
		assertEquals(vecinosCiudad3, mapa.getVecinos(ciudad3));
	}
	
	@Test 
	public void getVecinosArray() 
	{
		Mapa mapa = new Mapa();
		Ciudad ciudad = new Ciudad("x", 0, 0);
		Ciudad ciudad2 = new Ciudad("y", 0, 0);
		Ciudad ciudad3 = new Ciudad("z", 0, 0);
		Ruta ruta = new Ruta(ciudad, ciudad2, 1, 1);
		Ruta ruta2 = new Ruta(ciudad, ciudad3, 1, 0);
		ArrayList<Ciudad> vecinosCiudad = new ArrayList<Ciudad>();
		ArrayList<Ciudad> vecinosCiudad2 = new ArrayList<Ciudad>();
		ArrayList<Ciudad> vecinosCiudad3 = new ArrayList<Ciudad>();
		
		vecinosCiudad.add(ciudad2);
		vecinosCiudad.add(ciudad3);
		vecinosCiudad2.add(ciudad);
		vecinosCiudad3.add(ciudad);
		
		mapa.addRuta(ruta);
		mapa.addRuta(ruta2);
		
		assertTrue(vecinosCiudad.equals(mapa.getVecinosArray(ciudad)));
		assertTrue(vecinosCiudad2.equals(mapa.getVecinosArray(ciudad2)));
		assertTrue(vecinosCiudad3.equals(mapa.getVecinosArray(ciudad3)));
	}
	
	@Test 
	public void getDistanciaReal() 
	{
		Mapa mapa = new Mapa();
		Ciudad BuenosAiresCap = new Ciudad("Buenos Aires", -34.59972, -58.38194);
		Ciudad Tokio = new Ciudad("Tokio", 35.65861, 139.74555);
		double distanciaGrafo = 0;
		double distanciaReal = 18391;
		
		distanciaGrafo = mapa.getDistanciaReal(BuenosAiresCap, Tokio);
		
		assertEquals(distanciaGrafo, distanciaReal, 50);
		//la formula da la distancia en linea recta aproximada.
		//esta varia segun el radio terrestre que varia segun la latitud-
		//para el programa se utilizo el radio medio, de ahi
		//el margen de error de kilometros.
	}
}
