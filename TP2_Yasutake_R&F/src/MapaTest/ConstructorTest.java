package MapaTest;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.junit.Test;

import Mapa.Ciudad;
import Mapa.Constructor;
import Mapa.Ruta;
import Mapa.Trayecto;

public class ConstructorTest 
{

	@Test
	public void ConstructorList() 
	{
		Ruta[][] matrizRutasCiudades = new Ruta[3][3];
		Ciudad ciudad = new Ciudad("x", 0, 0);
		Ciudad ciudad2 = new Ciudad("y", 0, 0);
		Ciudad ciudad3 = new Ciudad("z", 0, 0);
		Ruta ruta = new Ruta(ciudad, ciudad2, 1, 1);
		Ruta ruta2 = new Ruta(ciudad, ciudad3, 1, 0);
		Ruta ruta3 = new Ruta(ciudad2, ciudad3, 1, 0);
		List<Integer> ciudades = new ArrayList<Integer>();
		Trayecto verificable = new Trayecto();
		Trayecto comparador = new Trayecto();
		
		ciudades.add(0);
		ciudades.add(1);
		comparador.addSubTrayecto(ruta);
		matrizRutasCiudades[0][1] = ruta;
		matrizRutasCiudades[1][0] = ruta;
		matrizRutasCiudades[0][2] = ruta2;
		matrizRutasCiudades[2][0] = ruta2;
		matrizRutasCiudades[1][2] = ruta3;
		matrizRutasCiudades[2][1] = ruta3;
		verificable = Constructor.generarTrayectoList(ciudades, matrizRutasCiudades);
		
		assertEquals(verificable.cantSubTrayectos(), comparador.cantSubTrayectos());
		assertEquals(verificable.getCiudadesVisitadas(), comparador.getCiudadesVisitadas());
		assertEquals(verificable.getDistanciaTotal(), comparador.getDistanciaTotal(), 0);
	}
	
	@Test
	public void ConstructorStack() 
	{
		Ruta[][] matrizRutasCiudades = new Ruta[3][3];
		Ciudad ciudad = new Ciudad("x", 0, 0);
		Ciudad ciudad2 = new Ciudad("y", 0, 0);
		Ciudad ciudad3 = new Ciudad("z", 0, 0);
		Ruta ruta = new Ruta(ciudad, ciudad2, 1, 1);
		Ruta ruta2 = new Ruta(ciudad, ciudad3, 1, 0);
		Ruta ruta3 = new Ruta(ciudad2, ciudad3, 1, 0);
		Stack<Integer> ciudades = new Stack<Integer>();
		Trayecto verificable = new Trayecto();
		Trayecto comparador = new Trayecto();
		
		ciudades.push(0);
		ciudades.push(1);
		comparador.addSubTrayecto(ruta);
		matrizRutasCiudades[0][1] = ruta;
		matrizRutasCiudades[1][0] = ruta;
		matrizRutasCiudades[0][2] = ruta2;
		matrizRutasCiudades[2][0] = ruta2;
		matrizRutasCiudades[1][2] = ruta3;
		matrizRutasCiudades[2][1] = ruta3;
		verificable = Constructor.generarTrayectoStack(ciudades, matrizRutasCiudades);
		
		assertEquals(verificable.cantSubTrayectos(), comparador.cantSubTrayectos());
		assertEquals(verificable.getCiudadesVisitadas(), comparador.getCiudadesVisitadas());
		assertEquals(verificable.getDistanciaTotal(), comparador.getDistanciaTotal(), 0);
	}
}
