package MapaTest;

import static org.junit.Assert.*;

import org.junit.Test;

import Mapa.Ciudad;
import Mapa.Ruta;
import Mapa.Trayecto;

public class TrayectoTest 
{

	@Test
	public void crearTrayecto() 
	{
		Trayecto trayecto = new Trayecto();
		
		assertEquals(0, trayecto.getPeajesTotales(), 0);
		assertEquals(0, trayecto.getDistanciaTotal(), 0);
		assertTrue(trayecto.getCiudadesVisitadas().isEmpty());
		assertTrue(trayecto.getCaminoMinimo().isEmpty());
	}
	
	@Test
	public void addSubtrayecto() 
	{
		Trayecto trayecto = new Trayecto();
		Ciudad ciudad = new Ciudad("x", 0, 0);
		Ciudad ciudad2 = new Ciudad("y", 0, 0);
		Ciudad ciudad3 = new Ciudad("z", 0, 0);
		Ruta ruta = new Ruta(ciudad, ciudad2, 1, 1);
		Ruta ruta2 = new Ruta(ciudad2, ciudad3, 1, 0);
		Ruta ruta3 = new Ruta(ciudad, ciudad3, 1, 0);
		
		trayecto.addSubTrayecto(ruta);
		trayecto.addSubTrayecto(ruta2);
		trayecto.addSubTrayecto(ruta3);
		
		assertEquals(1, trayecto.getPeajesTotales(), 0);
		assertEquals(3, trayecto.getDistanciaTotal(), 0);
		assertEquals(3, trayecto.getCaminoMinimo().size());
		assertEquals(3, trayecto.getCiudadesVisitadas().size());
	}
}
